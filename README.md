# SporzBot

Bot that runs [Sporz](https://sporz.fr) games.

The bot has almost zero configuration, you just need to put the token in config.json, install the module and run with `start.py`.

It depends on [discord.py](https//discordpy.readthedocs.io/) and `aiofiles`.
