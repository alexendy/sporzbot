

from enum import Enum
import discord

class Genome(Enum):
    HOST = -1
    NORMAL = 0
    RESIST = 1

genome_desc = {
    Genome.HOST : "hôte :sweat_smile:",
    Genome.NORMAL : "normal :ok_hand:",
    Genome.RESIST : "résistant :muscle:"}


class MutantMinorAction(Enum):
    M1_UNDEF = 0
    M1_PARALYZE = 1

class MutantMajorAction(Enum):
    M2_UNDEF = 0
    M2_MUTATE = 1
    M2_KILL = 2

class DoctorAction(Enum):
    DR_UNDEF = 0
    DR_CURE = 1
    DR_KILL = 2



class Astronaut:
    def __init__(self, game, player: discord.Member, mutant: bool = False, genome: Genome = Genome.NORMAL):
        self.player = player
        self.isMutant = mutant
        self.isDead = False
        self.isParalyzed = False
        self.isCaptain = False
        self.genome = genome
        self.game = game
        #self.roleName = "Astronaute"
        
        # The following are set to False
        self.mutantMinorActionAvailable = False # Paralyze
        self.mutantMajorActionAvailable = False # Mutate or kill
        self.doctorActionAvailable = False # Cure or kill
        self.phase1ActionAvailable = False
        self.phase2ActionAvailable = False
        self.voteAvailable = False
        
        self.spyLog = list()
    
    
        self.mutantMinorAction = MutantMinorAction.M1_UNDEF
        self.mutantMinorTarget = None
        
        self.mutantMajorAction = MutantMajorAction.M2_UNDEF
        self.mutantMajorTarget = None
    
    def getRole(self,start_phase=False): # By default, a mutant astronaut's role is just an astronaut. It's only during the start phase that we want to consider "base mutant" a role.
        if start_phase and self.isMutant:
            return "Mutanx de base !!! :biohazard::alien:"
        else:
            return "Astronaute :astronaut::rocket:"
    
    def getGenome(self):
        return genome_desc[self.genome]
    
    def getMutationState(self):
        return 'mutanx :biohazard:' if self.isMutant else 'sain :person_curly_hair:'
    
    def getName(self):
        n = self.player.display_name
        if self.isCaptain:
            n += ":military_medal:"
        return n
    
    def autopsy(self):
        return f"**{self.getRole()}**, **{self.getMutationState()}**, de génome **{self.getGenome()}**"
        
    async def kill(self):
        self.isDead = True
        self.mutantMinorActionAvailable = False
        self.mutantMajorActionAvailable = False
        self.doctorActionAvailable = False
        self.phase1ActionAvailable = False
        self.phase2ActionAvailable = False
        self.voteAvailable = False
        await self.game.process_death(self)
        #self.isCaptain = False # We must keep a track of captaincy to elect a new captain
        #self.isParalyzed = False
    
    
    def prepareForNight(self):
        self.mutantMinorAction = MutantMinorAction.M1_UNDEF
        self.mutantMinorTarget = None
        
        self.mutantMajorAction = MutantMajorAction.M2_UNDEF
        self.mutantMajorTarget = None
        
        self.isParalyzed = False
        
        self.spyLog = list()
        
        if self.isDead:
            return
        
        if self.isMutant:
            self.mutantMinorActionAvailable = True
            self.mutantMajorActionAvailable = True

    async def action_kill_mutant(self, index):
        targets = self.game.alive_people()
        if index <= 0 or index > len(targets):
            await self.game.current_mutants_channel().send(f"Merci d'indiquer une cible valide")
            return
        self.mutantMajorAction = MutantMajorAction.M2_KILL
        self.mutantMajorTarget = targets[index-1]
        await self.game.current_mutants_channel().send(f"Proposition de {self.getName()} enregistrée : **tuer** :skull: {self.mutantMajorTarget.getName()}")

    async def action_mutate(self, index):
        targets = self.game.alive_people()
        if index <= 0 or index > len(targets):
            await self.game.current_mutants_channel().send(f"Merci d'indiquer une cible valide")
            return
        self.mutantMajorAction = MutantMajorAction.M2_MUTATE
        self.mutantMajorTarget = targets[index-1]
        await self.game.current_mutants_channel().send(f"Proposition de {self.getName()} enregistrée : **muter** :biohazard: {self.mutantMajorTarget.getName()}")

    async def action_paralyze(self, index):
        targets = self.game.alive_people()
        if index <= 0 or index > len(targets):
            await self.game.current_mutants_channel().send(f"Merci d'indiquer une cible valide")
            return
        self.mutantMinorAction = MutantMinorAction.M1_PARALYZE
        self.mutantMinorTarget = targets[index-1]
        await self.game.current_mutants_channel().send(f"Proposition de {self.getName()} enregistrée : **paralyser** :confounded: {self.mutantMinorTarget.getName()}")


    async def paralyze(self):
        await self.player.send("Vous avez été paralysæ ! :confounded: Si vous avez un rôle, vous ne pourrez pas le jouer cette nuit.")
        self.isParalyzed = True
        self.doctorActionAvailable = False
        self.phase1ActionAvailable = False
        self.phase2ActionAvailable = False
        self.spyLog.append("paralysie")


    async def mutate(self):
        if self.isMutant:
            await self.player.send("Vous avez été mutæ ! :biohazard: Vous étiez déjà mutanx, donc ça n'a aucun effet.")
            await self.game.logs_channel.send(f"{self.getName()} a été mutæ. Cela n'a aucun effet, car al est déjà mutanx.")
        elif self.genome == Genome.RESIST:
            await self.player.send("On a tenté de vous muter... mais vous avez un **génome résistant ! :muscle:** La mutation échoue, vous êtes toujours sain.e.")
            await self.game.logs_channel.send(f"{self.getName()} a été mutæ. Cela a échoué, car al a un génome résistant !")
        else:
            await self.player.send("**Vous avez été mutæ et cela a réussi ! :biohazard:** Vous jouez maintenant dans le camp des mutants, et devez faire tout ce qui est en votre pouvoir pour les faire gagner.")
            await self.game.logs_channel.send(f"{self.getName()} a été mutæ. Cela a réussi !")
            self.isMutant = True
            self.doctorActionAvailable = False
        self.spyLog.append("mutation")



    async def mutant_kill(self):
            await self.player.send("**Vous avez été tuæ par les mutants ! :biohazard::skull:**")
            await self.game.logs_channel.send(f"{self.getName()} a été tuæ par les mutants ! :biohazard::skull: Al était : {self.autopsy()}")
            await self.game.main_channel.send(f"{self.getName()} a été tuæ par les mutants ! :biohazard::skull:")
            await self.kill()
            self.game.night_deaths.append(self)
            self.spyLog.append("meurtre par les mutants")

    async def doctor_kill(self):
            await self.player.send("**Vous avez été tuæ par les médecins ! :syringe::skull:**")
            await self.game.logs_channel.send(f"{self.getName()} a été tuæ par les médecins ! :syringe::skull: Al était : {self.autopsy()}")
            await self.game.main_channel.send(f"{self.getName()} a été tuæ par les médecins ! :syringe::skull:")
            await self.kill()
            self.game.night_deaths.append(self)
            self.spyLog.append("meurtre par les médecins")

    async def cure(self):
        if not self.isMutant:
            await self.player.send("Vous avez été soignæ ! :syringe: Vous n'étiez pas mutanx, donc ça n'a aucun effet.")
            await self.game.logs_channel.send(f"{self.getName()} a été soignæ. Cela n'a aucun effet, car al n'était pas mutanx.")
        elif self.genome == Genome.HOST:
            await self.player.send("On a tenté de vous soigner... mais vous avez un **génome hôte ! :smirk:** Le soin échoue, vous êtes toujours mutanx")
            await self.game.logs_channel.send(f"{self.getName()} a été soignæ. Cela a échoué, car al a un génome hôte !")
        else:
            await self.player.send("**Vous avez été soignæ et cela a réussi ! :syringe:** Vous êtes maintenant à nouveau sain.e, et devez faire tout ce qui est en votre pouvoir pour faire gagner le camp des sains.")
            await self.game.logs_channel.send(f"{self.getName()} a été soignæ. Cela a réussi ! :syringe:")
            self.isMutant = False
        self.spyLog.append("soin")

class Doctor(Astronaut):
    def __init__(self, game, player: discord.Member):
        Astronaut.__init__(self, game, player) # Doctors are not (initially) mutant and have normal genome
        self.drAction = DoctorAction.DR_UNDEF
        self.drActionTarget = None

    def getRole(self, start_phase=False):
        return "Médecin :syringe::medical_symbol:"

    async def mutate(self):
        await Astronaut.mutate(self)
        self.doctorActionAvailable = False


    def prepareForNight(self):
        Astronaut.prepareForNight(self)
        self.drAction = DoctorAction.DR_UNDEF
        self.drActionTarget = None
        if not self.isDead and not self.isMutant:
            self.doctorActionAvailable = True

    async def action_kill_doctor(self, index):
        targets = self.game.alive_people()
        if index <= 0 or index > len(targets):
            await self.game.doctor_channel.send(f"Merci d'indiquer une cible valide")
            return
        self.drAction = DoctorAction.DR_KILL
        self.drActionTarget = targets[index-1]
        await self.game.doctor_channel.send(f"Proposition d'action de {self.getName()} enregistrée : **tuer** :skull: {self.drActionTarget.getName()}")

    async def action_cure(self, index):
        targets = self.game.alive_people()
        if index <= 0 or index > len(targets):
            await self.game.doctor_channel.send(f"Merci d'indiquer une cible valide")
            return
        self.drAction = DoctorAction.DR_CURE
        self.drActionTarget = targets[index-1]
        await self.game.doctor_channel.send(f"Proposition d'action de {self.getName()} enregistrée : **soigner** :syringe: {self.drActionTarget.getName()}")


class Psychologist(Astronaut):
    def __init__(self, game, player: discord.Member):
        Astronaut.__init__(self, game, player)
        self.psyTarget = None
        self.psyResult = None
    
    def getRole(self, start_phase=False):
        return "Psychologue :brain::notebook_with_decorative_cover:"
    
    def prepareForNight(self):
        Astronaut.prepareForNight(self)
        self.psyTarget = None
        self.psyResult = None
        if not self.isDead:
            self.phase1ActionAvailable = True
    
    async def preparePhase1(self): # Called when phase 1 actions start
        await self.player.send(":brain::notebook_with_decorative_cover: Vous pouvez maintenant **inspecter an autre jouaire** (`+inspect <id>`) pour apprendre son état de mutation.")
        await self.player.send(self.game.get_options_alive("inspect"))
    
    async def inspect(self, target: int):
        options = self.game.alive_people()
        if target <= 0 or target > len(options):
            await self.player.send(f"Merci d'indiquer une cible valide")
            return
        self.psyTarget = options[target-1]
        self.psyResult = self.psyTarget.getMutationState()
        self.psyTarget.spyLog.append("inspection par an psychologue")
        await self.player.send(f"**Vous étudiez le comportement de {self.psyTarget.getName()}. Vous obtenez la certitude qu'al est, à cet instant, {self.psyResult}**")
        await self.game.logs_channel.send(f":brain::notebook_with_decorative_cover: {self.getName()} a inspecté **{self.psyTarget.getName()}** Résultat : **{self.psyResult}**")
        self.phase1ActionAvailable = False

    def hackResult(self):
        if self.psyTarget is None:
            return "Aucune information disponible."
        else:
            return f"{self.psyTarget.getName()} est {self.psyResult}"


class Geneticist(Astronaut):
    def __init__(self, game, player: discord.Member):
        Astronaut.__init__(self, game, player)
        self.geneTarget = None
        self.geneResult = None

    def getRole(self, start_phase=False):
        return "Génétician :scientist::dna:"
        
    def prepareForNight(self):
        Astronaut.prepareForNight(self)
        self.geneTarget = None
        self.geneResult = None
        if not self.isDead:
            self.phase1ActionAvailable = True

    async def preparePhase1(self): # Called when phase 1 actions start
        await self.player.send(":scientist::dna: Vous pouvez maintenant **inspecter an autre jouaire** (`+inspect <id>`) pour apprendre son génome.")
        await self.player.send(self.game.get_options_alive("inspect"))
    
    async def inspect(self, target: int):
        options = self.game.alive_people()
        if target <= 0 or target > len(options):
            await self.player.send(f"Merci d'indiquer une cible valide")
            return
        self.geneTarget = options[target-1]
        self.geneResult = self.geneTarget.getGenome()
        self.geneTarget.spyLog.append("inspection par an génétician")
        await self.player.send(f"**Vous séquencez le génome de {self.geneTarget.getName()}. Vous obtenez la certitude que celui-ci est de type {self.geneResult}**")
        await self.game.logs_channel.send(f":scientist::dna: {self.getName()} a inspecté **{self.geneTarget.getName()}** Résultat : **{self.geneResult}**")
        self.phase1ActionAvailable = False
    
    def hackResult(self):
        if self.geneTarget is None:
            return "Aucune information disponible."
        else:
            return f"{self.geneTarget.getName()} est de génome {self.geneResult}"


class ComputerScientist(Astronaut):
    def __init__(self, game, player: discord.Member):
        Astronaut.__init__(self, game, player)
        self.n_mutants = None

    def getRole(self, start_phase=False):
        return "Informatician :computer::zero::one:"


    def prepareForNight(self):
        Astronaut.prepareForNight(self)
        self.n_mutants = None
        if not self.isDead:
            self.phase1ActionAvailable = True
    
    async def preparePhase1(self): # Called when phase 1 actions start
        self.n_mutants = len(self.game.alive_mutants())
        await self.player.send(f":computer::zero::one: Vous faites de savants calculs statistiques qui vous apprennent qu'al y a **{self.n_mutants} mutanz à bord !**")
        await self.game.logs_channel.send(f":computer::zero::one: {self.getName()} a appris le nombre de mutants à bord. Résultat : **{self.n_mutants}**")
        self.phase1ActionAvailable = False
        # No need to check end of phase 1 here, this will be done in Game after preparePhase1 is called for everyone
    
    async def inspect(self, target: int):
        await self.player.send(f"**Vous êtes informatician, vous ne pouvez inspecter personne.")

    def hackResult(self):
        if self.n_mutants is None:
            return "Aucune information disponible."
        else:
            return f"{self.n_mutants} mutanz à bord."

        
class Hacker(Astronaut):
    def __init__(self, game, player: discord.Member, hackables: dict):
        Astronaut.__init__(self, game, player)
        self.hackTarget = None
        self.oldHackTarget = None
        self.hackables = hackables
    
    def getRole(self, start_phase=False):
        return "Hacker :desktop::fire:"
    
    def prepareForNight(self):
        Astronaut.prepareForNight(self)
        self.oldHackTarget = self.hackTarget
        self.hackTarget = None
        if not self.isDead:
            self.phase2ActionAvailable = True

    async def preparePhase2(self): # Called when phase 1 actions start
        await self.player.send(f":desktop::fire: Vous pouvez maintenant **pirater les données** d'un rôle à information pour apprendre ce qu'al a appris cette nuit. Vous ne pouvez pas pirater deux fois de suite le même rôle, sauf si vous n'avez pas le choix.")
        options = ", ".join([f"+hack {role}" for role in self.hackables.keys()])
        await self.player.send(f"Options : {options}")
        
    async def hack(self, target: str): # Called when phase 1 actions start
        if target not in self.hackables:
            await self.player.send("Choix invalide.")
            return
        target_char = self.hackables[target]
        if target_char == self.oldHackTarget and (len(self.hackables)>1):
            await self.player.send("Vous ne pouvez pas pirater deux nuits de suite le même rôle, faites un autre choix.")
            return
        self.hackTarget = target_char
        await self.player.send(f"Vous piratez les dossiers du rôle {self.hackTarget.getRole()}. Vous apprenez les informations suivantes : {self.hackTarget.hackResult()}")
        await self.game.logs_channel.send(f":desktop::fire: {self.getName()} a hacké le rôle `{target}` (c'est à dire {self.hackTarget.getName()}) Résultat : {self.hackTarget.hackResult()}")
        self.phase2ActionAvailable = False
        

class Spy(Astronaut):
    def __init__(self, game, player: discord.Member):
        Astronaut.__init__(self, game, player)
        self.spyTarget = None
    
    def getRole(self, start_phase=False):
        return "Espion.ne :detective::eye:"
    
    def prepareForNight(self):
        Astronaut.prepareForNight(self)
        self.spyTarget = None
        if not self.isDead:
            self.phase2ActionAvailable = True

    async def preparePhase2(self): # Called when phase 1 actions start
        await self.player.send(":detective::eye: Vous pouvez maintenant **inspecter an autre jouaire** (`+inspect <id>`) pour savoir ce qui lui est arrivé cette nuit.")
        await self.player.send(self.game.get_options_alive("spy"))
    
    async def spy(self, target: int):
        options = self.game.alive_people()
        if target <= 0 or target > len(options):
            await self.player.send(f"Merci d'indiquer une cible valide")
            return
        self.spyTarget = options[target-1]
        log = self.spyTarget.spyLog
        if len(log) == 0:
            spyResult = "Al ne lui est rien arrivæ de particulier cette nuit."
        else:
            spyResult = "Voici ce qui lui est arrivæ cette nuit : "+(", ".join(log))
        await self.player.send(f"**Vous espionnez {self.spyTarget.getName()}.** {spyResult}")
        await self.game.logs_channel.send(f":detective::eye: {self.getName()} a espionné **{self.spyTarget.getName()}** Résultat : {spyResult}")
        self.phase2ActionAvailable = False
