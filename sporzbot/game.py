
import discord
from discord.ext import commands
from enum import Enum
import random

import asyncio

from sporzbot.gamesetup import gen_parameters, GameParameter, MIN_PLAYERS, MIN_PLAYERS_WARNING, MAX_PLAYERS_WARNING
from sporzbot.roles import *
from sporzbot.vote import Vote, SpecialVote

class ChannelType(Enum):
    CHANNEL_NOT_IN_GAME = -1
    CHANNEL_PRESETUP = 0
    CHANNEL_MAIN = 1
    CHANNEL_DOCTOR = 2
    CHANNEL_MUTANT_OLD = 3
    CHANNEL_MUTANT_CURRENT = 4
    CHANNEL_LOG = 5
    CHANNEL_DEAD = 6

class GameState(Enum):
    STATE_PRESETUP = 0 # Player join
    STATE_SETUP = 1 # Game organizer sets parameters
    STATE_ELECT_CAPTAIN = 2 # Initial captain election
    STATE_NIGHT_MUTANT = 3 # Mutant turn
    STATE_NIGHT_DOCTOR = 4 # Doctors turn
    STATE_NIGHT_ROLE1 = 5 # Psychologist/Geneticist/ComputerScientist turn
    STATE_NIGHT_ROLE2 = 6 # Hacker/Spy turn
    STATE_DAY_MORNING = 8 # Tell dead people, nominate new captain if previous dead
    STATE_DAY_VOTE = 7 # Day vote
    STATE_DAY_EVENING = 8 # Nominate new captain if previous killed by vote
    STATE_ENDGAME = 9





channel_types_desc = {
    ChannelType.CHANNEL_NOT_IN_GAME : "Canal en dehors d'une partie",
    ChannelType.CHANNEL_PRESETUP : "Canal avec une partie en cours de démarrage",
    ChannelType.CHANNEL_MAIN : "Canal principal de la partie",
    ChannelType.CHANNEL_DOCTOR : "Canal secret des médecins",
    ChannelType.CHANNEL_MUTANT_OLD : "Canal secret des mutants (archivé)",
    ChannelType.CHANNEL_MUTANT_CURRENT : "Canal secret des mutants",
    ChannelType.CHANNEL_LOG : "Canal d'historique de la partie",
    ChannelType.CHANNEL_DEAD : "Canal de discussion des morts"}
    





class Game:
    def __init__(self, channel: discord.TextChannel, organizer:discord.Member, manager: 'GamesManager'):
        self.manager = manager
        self.server = channel.guild
        self.prechannel = channel
        self.organizer = organizer
        self.players = [organizer]
        self.state = GameState.STATE_PRESETUP
        self.category = None
        self.main_channel = None
        self.voice_channel = None
        self.doctor_channel = None
        self.mutant_channels = list()
        self.logs_channel = None
        self.dead_channel = None
        self.current_day = 0
        self.captain = None
        self.characters = list()
        self.parameters = None
        self.inactive_doctor_status_known = None
        
        self.doctors = list()
        
        self.night_deaths = list() # Store the dead during the night, to display the autopsy in morning
        
        self.last_day_victim = None

        self.current_vote = None

        #Register in manager
        self.manager.game_per_channel_id[self.prechannel.id] = (self, ChannelType.CHANNEL_PRESETUP)
        self.manager.game_per_user_id[self.organizer.id] = (self,  None) # No role yet


    def current_mutants_channel(self):
        return self.mutant_channels[-1]

    def mutants(self): # Mutants, alive or not
        return list(filter(lambda c: c.isMutant, self.characters))

    def alive_mutants(self): # Mutants that are not dead
        return list(filter(lambda c: c.isMutant and not c.isDead, self.characters))

    def alive_healthy(self): # Alive people who are not mutant
        return list(filter(lambda c: not c.isMutant and not c.isDead, self.characters))

    def active_doctors(self): # Doctors that are not currently dead, mutated or paralyzed
        return list(filter(lambda dr: not (dr.isDead or dr.isMutant or dr.isParalyzed), self.doctors))

    def inactive_doctors(self): # Doctors that are not currently dead, mutated or paralyzed
        return list(filter(lambda dr: (dr.isDead or dr.isMutant or dr.isParalyzed), self.doctors))

    def alive_people(self): # Crew that are not dead
        return list(filter(lambda c: not c.isDead, self.characters))

    def dead_people(self): # Crew that are dead
        return list(filter(lambda c: c.isDead, self.characters))

    def add_player(self, player: discord.Member):
        self.players.append(player)
        self.manager.game_per_user_id[player.id] = (self, None) # No role yet
    
    async def start(self):
        if(len(self.players) < MIN_PLAYERS):
            await self.prechannel.send(f"Au moins {MIN_PLAYERS} jouaires sont nécessaires pour démarrer une partie (et au moins {MIN_PLAYERS_WARNING} sont recommandés)")
            return
        
        if(len(self.players) < MIN_PLAYERS_WARNING):
            await self.prechannel.send(f"Attention, il est recommandé d'avoir au moins {MIN_PLAYERS_WARNING} jouaires")
        
        if(len(self.players) > MAX_PLAYERS_WARNING):
            await self.prechannel.send(f"Attention, il est recommandé d'avoir au plus {MAX_PLAYERS_WARNING} jouaires")
        
        self.state = GameState.STATE_SETUP

        # Detach from the prechannel - allows someone else to start another game if they want
        del self.manager.game_per_channel_id[self.prechannel.id]
        self.prechannel = None
        
        for j in self.players:
            await j.send(":biohazard::alien: **:warning: La partie de Sporz :biohazard: que vous avez rejoint est sur le point de commencer !**")
        
        # Setup main channel :
        permissions_category = {
            player: discord.PermissionOverwrite(view_channel=True)
            for player in self.players
        }
        permissions_category[self.server.me] = discord.PermissionOverwrite(view_channel=True) # Add myself !
        permissions_category[self.server.default_role] = discord.PermissionOverwrite(view_channel=False)
        self.category = await self.server.create_category("Partie Sporz", overwrites=permissions_category)
        self.main_channel = await self.server.create_text_channel("sporz-general", category=self.category)
        self.manager.game_per_channel_id[self.main_channel.id] = (self, ChannelType.CHANNEL_MAIN)
        
        await self.main_channel.send(f":biohazard::alien: **Une nouvelle partie a été initialisée par {self.organizer.display_name}**")
        await self.main_channel.send("Jouaires : {}".format(', '.join(p.display_name for p in self.players)))
        
        # Setup voice channel
        self.voice_channel = await self.server.create_voice_channel("sporz-vocal", category=self.category)
        
        # Get default parameters
        self.parameters = gen_parameters(len(self.players))
        await self.main_channel.send(f"__Paramètres suggérés pour {len(self.players)} jouaires :__")
        await self.main_channel.send("\n".join([p.get() for p in self.parameters.values()]))
        await self.main_channel.send(f"{self.organizer.display_name}, modifiez les paramètres de votre choix avec `+set <paramètre> <valeur>` puis lancez la partie avec `+game begin`")
        

    def captainElectionCallback(self, elected):
        char, name = elected
        self.current_vote = None # no vote any more
        char.isCaptain = True
        self.captain = char
        asyncio.create_task(self.start_first_night()) # Create task to launch first night !

    async def start_first_night(self):
        await self.lock_main_channel() # Make the main channel read only for night
        await self.main_channel.send(f"**{self.captain.getName()} est maintenant votre Capitaine :military_medal:**")
        await self.logs_channel.send(f"{self.captain.getName()} a été élu Capitaine :military_medal:")
        #Create doctor and dead channels
        await self.create_doctors_channel()
        await self.create_dead_channel()
        await self.new_night()


    async def begin(self):
        await self.main_channel.send(f"__Récapitulatif des paramètres :__")
        await self.main_channel.send("\n".join([p.get() for p in self.parameters.values()]))
        
        n_players = len(self.players)
        
        
        # Sanity checks
        n_mutants = self.parameters["n_mutants"].value
        n_doctors = self.parameters["n_doctors"].value
        n_psy = self.parameters["n_psy"].value
        n_gen = self.parameters["n_gen"].value
        n_computer = self.parameters["n_computer"].value
        n_hacker = self.parameters["n_hacker"].value
        n_spy = self.parameters["n_spy"].value
        
        n_extra_hosts = self.parameters["n_extra_hosts"].value
        n_resist = self.parameters["n_resist"].value
        
        self.inactive_doctor_status_known = (self.parameters["doctor_status_known"].value > 0)

        n_astronauts = n_players - (n_mutants + n_doctors + n_psy + n_gen + n_computer + n_hacker + n_spy)
        if(n_astronauts < 0):
            await self.main_channel.send(f"Pas assez de jouaires pour cette répartition de rôles ! Enlevez au moins {-n_astronauts} rôles.")
            return
        
        n_free_genomes = n_players - (n_mutants + n_doctors)
        if(n_free_genomes < (n_extra_hosts + n_resist)):
            await self.main_channel.send("Pas assez de jouaires pour cette répartition de génomes !")
            return
        
        await self.main_channel.send(f"Al y aura {n_astronauts} simples astronautes.")
        
        hackables = dict()
        #Roles distribution
        indices = random.sample(range(n_players),k=n_players)
        for i in range(0, n_mutants):
            mutant = Astronaut(self, self.players[indices[i]], True, Genome.HOST)
            self.characters.append(mutant)
            self.manager.game_per_user_id[mutant.player.id] = (self, mutant)
        n = n_mutants
        for i in range(n, n+n_doctors):
            dr = Doctor(self, self.players[indices[i]])
            self.characters.append(dr)
            self.doctors.append(dr)
            self.manager.game_per_user_id[dr.player.id] = (self, dr)
        n += n_doctors
        for i in range(n,n+n_psy):
            p = Psychologist(self, self.players[indices[i]])
            self.characters.append(p)
            self.manager.game_per_user_id[p.player.id] = (self, p)
            if "psy" not in hackables:
                hackables["psy"] = p
        n += n_psy
        for i in range(n,n+n_gen):
            p = Geneticist(self, self.players[indices[i]])
            self.characters.append(p)
            self.manager.game_per_user_id[p.player.id] = (self, p)
            if "gen" not in hackables:
                hackables["gen"] = p
        n += n_gen
        for i in range(n,n+n_computer):
            p = ComputerScientist(self, self.players[indices[i]])
            self.characters.append(p)
            self.manager.game_per_user_id[p.player.id] = (self, p)
            if "info" not in hackables:
                hackables["info"] = p
        n += n_computer
        for i in range(n,n+n_hacker):
            p = Hacker(self, self.players[indices[i]], hackables)
            self.characters.append(p)
            self.manager.game_per_user_id[p.player.id] = (self, p)
        n += n_hacker
        for i in range(n,n+n_spy):
            p = Spy(self, self.players[indices[i]])
            self.characters.append(p)
            self.manager.game_per_user_id[p.player.id] = (self, p)
        n += n_spy
        for i in range(n,n+n_astronauts):
            p = Astronaut(self, self.players[indices[i]])
            self.characters.append(p)
            self.manager.game_per_user_id[p.player.id] = (self, p)
        
        #Handle genomes
        idx_genomes = random.sample(range(n_mutants+n_doctors, n_players), n_extra_hosts + n_resist) # Random sample of characteers outside mutants and doctors
        for i in range(n_extra_hosts):
            self.characters[idx_genomes[i]].genome = Genome.HOST
        for i in range(n_extra_hosts, n_extra_hosts+n_resist):
            self.characters[idx_genomes[i]].genome = Genome.RESIST

        # Shuffle characters
        random.shuffle(self.characters)

        # Setup log channel
        permissions_only_me = {
            self.server.me : discord.PermissionOverwrite(view_channel=True),
            self.server.default_role : discord.PermissionOverwrite(view_channel=False)
        }
        self.logs_channel = await self.server.create_text_channel("journal-partie", category=self.category, overwrites=permissions_only_me)
        self.manager.game_per_channel_id[self.logs_channel.id] = (self, ChannelType.CHANNEL_LOG)
        
        # Write full game setup in log channel
        await self.logs_channel.send("**__:biohazard: Démarrage partie :biohazard:__**")
        await self.logs_channel.send("__Distribution des rôles :__")
        await self.logs_channel.send("\n".join(["- {} : {} (génome {})".format(c.getName(), c.getRole(start_phase=True), c.getGenome()) for c in self.characters]))
        
        # Send everyone their role
        for c in self.characters:
            await c.player.send(f"Vous avez reçu le rôle : **{c.getRole(start_phase=True)}**")
        
        # Tell the doctors their colleague
        if len(self.doctors) > 1:
            doctors_str = ", ".join([d.getName() for d in self.doctors])
            for dr in self.doctors:
                await dr.player.send(f":syringe: Les médecins sur cette partie sont : {doctors_str}")

        # Tell the mutants their colleague if they are more than one
        if len(self.mutants()) > 1:
            mutants_str = ",".join([d.getName() for m in self.mutants()])
            for m in self.mutants():
                await m.player.send(f":biohazard: Les mutants de base sont : {mutants_str}")


        # Go on to electing the captain
        await self.main_channel.send(f"**__:biohazard::alien: === DEBUT DU JEU === :alien::biohazard:__**")
        await self.main_channel.send(f"**Election d'an Capitaine :military_medal:**")


        self.current_vote = Vote("Election d'an Capitaine",
            self.captainElectionCallback,
            self.main_channel,
            options=[(ch, ch.getName()) for ch in self.characters],
            voters=self.players,
            tiebreaker=None,
            blank_allowed=False,
            abstain_allowed=True)

        self.state = GameState.STATE_ELECT_CAPTAIN
        await self.current_vote.start()



    async def abort(self):
        if self.prechannel:
            del self.manager.game_per_channel_id[self.prechannel.id]
        if self.logs_channel:
            del self.manager.game_per_channel_id[self.logs_channel.id]
            await self.logs_channel.delete()
        if self.dead_channel:
            del self.manager.game_per_channel_id[self.dead_channel.id]
            await self.dead_channel.delete()
        if self.voice_channel:
            await self.voice_channel.delete()
        if self.doctor_channel:
            del self.manager.game_per_channel_id[self.doctor_channel.id]
            await self.doctor_channel.delete()
        for c in self.mutant_channels:
            del self.manager.game_per_channel_id[c.id]
            await c.delete()
        for p in self.players:
            try:
                del self.manager.game_per_user_id[p.id]
            except KeyError: # TODO Remove when tests with multiple characters per account over
                pass
        if self.category:
            await self.category.delete()
        if self.main_channel:
            del self.manager.game_per_channel_id[self.main_channel.id]
            await self.main_channel.delete()



    def getparam(self,name: str):
        try:
            return self.parameters[name]
        except KeyError:
            return None
            
    
            
    #Return a message that can be used to give the option to choose someone among the living people
    def get_options_alive(self, command=None):
        string = "__Cibles possibles :__\n"
        if command:
            string += "\n".join([f"`+{command} {i+1}` : {ch.getName()}" for (i,ch) in enumerate(self.alive_people())])
        else:
            string += "\n".join([f"`{i+1}` : {ch.getName()}" for (i,ch) in enumerate(self.alive_people())])
        return string
    
    # ==== PRIVATE CHANNELS MANAGEMENT ====
    
    # Create the doctors channel with nobody inside initially, the active doctors will be added and removed each night
    async def create_doctors_channel(self): 
        permissions_doctors = {
            self.server.me : discord.PermissionOverwrite(view_channel=True),
            self.server.default_role : discord.PermissionOverwrite(view_channel=False)
        }
        self.doctor_channel = await self.server.create_text_channel("canal-medecins", category=self.category, overwrites=permissions_doctors)
        self.manager.game_per_channel_id[self.doctor_channel.id] = (self, ChannelType.CHANNEL_DOCTOR)
    
    # Create the dead people channel with nobody inside initially, the active doctors will be added and removed each night
    async def create_dead_channel(self):
        permissions_dead = {
            self.server.me : discord.PermissionOverwrite(view_channel=True),
            self.server.default_role : discord.PermissionOverwrite(view_channel=False)
        }
        self.dead_channel = await self.server.create_text_channel("discussion-morts", category=self.category, overwrites=permissions_dead)
        self.manager.game_per_channel_id[self.dead_channel.id] = (self, ChannelType.CHANNEL_DEAD)
    
    # Lock the main channel (for night)
    async def lock_main_channel(self):
        for player in self.players:
            await self.main_channel.set_permissions(player, view_channel=True, send_messages=False, add_reactions=False) # Read  only
        await self.mute_all()

    # Unlock the main channel (after night)
    async def unlock_main_channel(self):
        for char in self.alive_people(): # Only alive people
            await self.main_channel.set_permissions(char.player, view_channel=True, send_messages=True, add_reactions=True) # Restore full permissions
        await self.unmute_alive()
    
    # Mute all players (for night)
    async def mute_all(self):
        for player in self.players:
            await self.voice_channel.set_permissions(player, connect=True, speak=False) # Listen  only
    
    # Unmute all alive players (after night)
    async def unmute_alive(self):
        for char in self.alive_people():
            await self.voice_channel.set_permissions(char.player, connect=True, speak=True) # Can speak again

    
    # Create a mutant channel for the night, with the living mutants
    async def create_mutant_channel(self):
        permissions_mutant = {
            char.player: discord.PermissionOverwrite(view_channel=True)
            for char in self.alive_mutants()
        }
        permissions_mutant[self.server.me] = discord.PermissionOverwrite(view_channel=True) # Add myself !
        permissions_mutant[self.server.default_role] = discord.PermissionOverwrite(view_channel=False)
        m = await self.server.create_text_channel(f"mutants-nuit{self.current_day}", category=self.category, overwrites=permissions_mutant)
        self.mutant_channels.append(m)
        self.manager.game_per_channel_id[m.id] = (self, ChannelType.CHANNEL_MUTANT_CURRENT)
    
    
    # Lock and archive current mutant channel
    async def lock_mutant_channel(self):
        for char in self.alive_mutants():
            await self.current_mutants_channel().set_permissions(char.player, view_channel=True, send_messages=False, add_reactions=False) # Read only
        self.manager.game_per_channel_id[self.current_mutants_channel()] = (self, ChannelType.CHANNEL_MUTANT_OLD)
    
    # Activate doctors channel, by adding the active doctors
    async def activate_doctors_channel(self):
        for char in self.active_doctors():
            await self.doctor_channel.set_permissions(char.player, view_channel=True, send_messages=True, add_reactions=True) # Give access
    
    # Inactivate doctors channel, by removing write rights from doctors
    async def inactivate_doctors_channel(self):
        for char in self.active_doctors():
            await self.doctor_channel.set_permissions(char.player, view_channel=True, send_messages=False, add_reactions=False) # Read only

    # Disable doctors channel, by removing everyone
    async def disable_doctors_channel(self):
        for char in self.characters:
            await self.doctor_channel.set_permissions(char.player, view_channel=False, send_messages=False, add_reactions=False) # Read only
    
    # Someone is dead, we must do the necessary with the channels
    async def process_death(self, character):
        # if we are during the day, pass main channel read only
        if self.state == GameState.STATE_DAY_MORNING or self.state == GameState.STATE_DAY_VOTE or self.state == GameState.STATE_DAY_EVENING:
            await self.main_channel.set_permissions(character.player, view_channel=True, send_messages=False, add_reactions=False)
        if not character.isCaptain: # This must be delayed for captains because they must first nominate their successors
            await self.add_to_death_channels(character)
        



    async def add_to_death_channels(self, character):
        await self.dead_channel.set_permissions(character.player, view_channel=True) # Full access to the dead channel
        await self.logs_channel.set_permissions(character.player, view_channel=True, send_messages=False, add_reactions=False) # Read only permission to the game log
        # Say hi
        await self.dead_channel.send(f"**__:skull: Accueillez comme al se doit {character.player.mention} qui vient de mourir !__**")




    
    # ==== NIGHT ACTIONS ====
    async def new_night(self):
        self.current_day += 1
        for ch in self.characters:
            ch.prepareForNight()
        await self.main_channel.send(f"**__:crescent_moon::night_with_stars: === NUIT {self.current_day} ===__**")
        await self.logs_channel.send(f"**__:crescent_moon::night_with_stars: === NUIT {self.current_day} ===__**")
        asyncio.create_task(self.setup_mutants_turn()) # Create task to launch mutant turn




    async def setup_mutants_turn(self):
        self.state = GameState.STATE_NIGHT_MUTANT
        alive_mutants_str = ", ".join([c.getName() for c in self.alive_mutants()])
        await self.create_mutant_channel()
        await self.current_mutants_channel().send(f"**Nuit {self.current_day} - Tour des mutants - Mutants en vie : {alive_mutants_str}**")
        await self.logs_channel.send(f"**:biohazard: Tour des mutants - Mutants en vie : {alive_mutants_str}**")
        await self.main_channel.send(f"Tour des mutanz :biohazard:")
        await self.current_mutants_channel().send(f"Vous devez discuter et vous coordonner pour :\n- **Paralyser :confounded:** (`+paralyze <numero>`) une cible\n- **Muter :biohazard:** (`+mutate <numero>`) ou **tuer :skull:** (`+kill <numero>`) une cible")
        await self.current_mutants_channel().send(self.get_options_alive())

    async def check_end_night_mutant(self):
        # Check that everyone proposed something for both actions:
        for m in self.alive_mutants():
            if m.mutantMinorAction == MutantMinorAction.M1_UNDEF or m.mutantMajorAction == MutantMajorAction.M2_UNDEF:
                return #Not every action proposed
        # Everyone has proposed something, now check if everyone agree
        minoraction = self.alive_mutants()[0].mutantMinorAction
        majoraction = self.alive_mutants()[0].mutantMajorAction
        minortarget = self.alive_mutants()[0].mutantMinorTarget
        majortarget = self.alive_mutants()[0].mutantMajorTarget
        for mutant in self.alive_mutants():
            if not (mutant.mutantMinorAction == minoraction and mutant.mutantMajorAction == majoraction and mutant.mutantMinorTarget == minortarget and mutant.mutantMajorTarget == majortarget):
                await self.current_mutants_channel().send("**Les actions seront validées quand tout le monde sera d'accord sur les deux actions**")
                return
        # Everybody agrees !
        await self.lock_mutant_channel()
        action_str = ("muter :biohazard:" if majoraction == MutantMajorAction.M2_MUTATE else "tuer :skull:")
        await self.current_mutants_channel().send(f"Action décidée : **paralyser :confounded:** {minortarget.getName()} et **{action_str}** {majortarget.getName()}")
        await self.logs_channel.send(f"__Action des mutants :__ **paralyser :confounded:** {minortarget.getName()} et **{action_str}** {majortarget.getName()}")
        await minortarget.paralyze()
        if majoraction == MutantMajorAction.M2_KILL:
            await majortarget.mutant_kill()
        else:
            await majortarget.mutate()
        asyncio.create_task(self.setup_doctors_turn()) # Create task to launch mutant turn

    async def setup_doctors_turn(self):
        self.state = GameState.STATE_NIGHT_DOCTOR
        active_drs_str = (", ".join([c.getName() for c in self.active_doctors()]) if (len(self.active_doctors())>0) else "aucun :slight_frown:")
        if self.inactive_doctor_status_known:
            inactive_drs_str = ", ".join([f"{c.getName()} ({c.getMutationState()})" for c in self.inactive_doctors()])
        else:
            inactive_drs_str = ", ".join([c.getName() for c in self.inactive_doctors()])
        await self.activate_doctors_channel()
        await self.logs_channel.send(f"**:syringe: Tour des médecins - Médecins actiz : {active_drs_str}**")
        await self.main_channel.send(f"Tour des médecins :syringe:")
        await self.doctor_channel.send(f"**Nuit {self.current_day} - Tour des médecins :syringe: - Médecins actiz : {active_drs_str}**")
        if len(self.inactive_doctors()) > 0:
            await self.logs_channel.send(f"**(Médecins inactiz : {inactive_drs_str})**")
            await self.doctor_channel.send(f"**(Médecins inactiz : {inactive_drs_str})**")
        if len(self.active_doctors()) > 0:
            await self.doctor_channel.send(f"Vous devez discuter et vous coordonner pour :\n- Soit **soigner :syringe:** (`+cure <numero>`) une cible __chacan__\n- Soit **tuer :skull:** (`+drkill <numero>`) une cible __en tout__")
            await self.doctor_channel.send(self.get_options_alive())
        else:
            await self.doctor_channel.send("Aucune action disponible cette nuit")
            await asyncio.sleep(10. + random.random()*10) # Random delay between 10 and 20s
            asyncio.create_task(self.setup_phase1_turn()) # Immediately go to phase 1 roles turn
        
    async def check_end_night_doctor(self):
        for dr in self.active_doctors():
            if dr.drAction == DoctorAction.DR_UNDEF:
                return #Not every action proposed
        # Everyone has proposed something, now check if everyone agree on a kill, or proposed a cure
        draction = self.active_doctors()[0].drAction
        for dr in self.active_doctors():
                if dr.drAction != draction: # Not everyone agrees on which action to take
                    await self.doctor_channel.send(f"Les actions seront validées quand 1) tout le monde aura déclaré un soin ou 2) tout le monde aura déclaré le **même** meurtre.")
                    return
        # Everybody has declared the same type of action (cure or kill).
        if draction == DoctorAction.DR_KILL: # If it is a kill, check everyone agrees on which kill
            drtarget_kill = self.active_doctors()[0].drActionTarget
            for dr in self.active_doctors():
                if dr.drActionTarget != drtarget_kill:
                    await self.doctor_channel.send(f"Les actions seront validées quand 1) tout le monde aura déclaré un soin ou 2) tout le monde aura déclaré le **même** meurtre.")
                    return
            # We have a valid kill target
            await self.inactivate_doctors_channel()
            await self.doctor_channel.send(f"Action décidée : **tuer** :skull: {drtarget_kill.getName()}")
            await self.logs_channel.send(f"__Action des médecins :__ **tuer** :skull: {drtarget_kill.getName()}")
            await drtarget_kill.doctor_kill()
        else: #draction == DoctorAction.DR_CURE
            await self.inactivate_doctors_channel()
            for dr in self.active_doctors():
                await self.doctor_channel.send(f"Action : {dr.getName()} a soigné :syringe: {dr.drActionTarget.getName()}")
                await self.logs_channel.send(f"__Action des médecins :__ {dr.getName()} a soigné :syringe: {dr.drActionTarget.getName()}")
                await dr.drActionTarget.cure()
        asyncio.create_task(self.setup_phase1_turn()) # Create task to launch phase 1 roles turn
        
    # Phase 1 roles
    async def setup_phase1_turn(self):
        self.state = GameState.STATE_NIGHT_ROLE1
        await self.logs_channel.send(f"**:brain::dna::computer: Tour des psychologues, généticians et informaticians :brain::dna::computer:**")
        await self.main_channel.send(f"Tour des psychologues, généticians et informaticians :brain::dna::computer:")
        for char in self.alive_people():
            if char.phase1ActionAvailable:
                await char.preparePhase1()
        await self.check_end_phase1() # If no role, or only computer scientists
        
    async def check_end_phase1(self):
        for char in self.alive_people():
            if char.phase1ActionAvailable:
                return # Not finished yet
        #Phase 1 over
        await asyncio.sleep(5. + random.random()*10) # Random delay between 5 and 15s
        asyncio.create_task(self.setup_phase2_turn()) # Create task to launch phase 1 roles turn

    # Phase 2 roles
    async def setup_phase2_turn(self):
        self.state = GameState.STATE_NIGHT_ROLE2
        await self.logs_channel.send(f"**:desktop::detective: Tour des hackers et espionz**")
        await self.main_channel.send(f"Tour des hackers et espionz :desktop::detective:")
        for char in self.alive_people():
            if char.phase2ActionAvailable:
                await char.preparePhase2()
        await self.check_end_phase2() # If no spy or hacker
    
    async def check_end_phase2(self):
        for char in self.alive_people():
            if char.phase2ActionAvailable:
                return # Not finished yet
        #Phase 2 over
        await asyncio.sleep(5. + random.random()*10) # Random delay between 5 and 15s
        asyncio.create_task(self.setup_end_night()) # Create task to launch phase 1 roles turn


    # The dying captain nominates a successor
    async def init_captain_choice(self):
        await self.captain.player.send("Vous étiez Capitaine :military_medal:, vous devez choisir votre successeur.")
        await self.captain.player.send(self.get_options_alive("captain"))



    async def nominate_captain(self, choice: int):
        targets = self.alive_people()
        if choice <= 0 or choice > len(targets):
            await self.captain.player.send(f"Merci d'indiquer une cible valide")
            return
        newCaptain = targets[choice-1]
        oldCaptain = self.captain
        self.captain = newCaptain
        oldCaptain.isCaptain = False # The dead captain is not captain any more
        newCaptain.isCaptain = True
        await self.main_channel.send(f":military_medal: L'ancian capitaine {oldCaptain.getName()} a nommé **{self.captain.player.mention} comme nouveau Capitaine !**")
        await self.logs_channel.send(f":military_medal: {oldCaptain.getName()}, capitaine mort, a choisi **{self.captain.getName()}** pour lui succéder.")
        await self.add_to_death_channels(oldCaptain)
        if(self.state == GameState.STATE_DAY_MORNING): # The captain died during the night, now proceed to the day vote
            asyncio.create_task(self.start_day_vote())
        elif(self.state == GameState.STATE_DAY_EVENING): # The captain died during the day vote, now proceed to the night
            asyncio.create_task(self.new_night())


    # End of night
    async def setup_end_night(self):
        self.state = GameState.STATE_DAY_MORNING
        await self.main_channel.send(f"**__:sun_with_face::city_sunset: === JOUR {self.current_day} ===__**")
        await self.logs_channel.send(f"**__:sun_with_face::city_sunset: === JOUR {self.current_day} ===__**")
        dead_captain = False
        for dead in self.night_deaths:
            await self.main_channel.send(f"**{dead.getName()}** est mort pendant la nuit. Al était : {dead.autopsy()}")
            if dead.isCaptain:
                dead_captain = True
        self.night_deaths.clear()
        await self.check_endgame()
        if not self.state == GameState.STATE_ENDGAME:
            await self.unlock_main_channel()
            if dead_captain:
                await self.main_channel.send(f"{self.captain.getName()} était votre capitaine, patientez pendant qu'al nomme son successeur...")
                await self.init_captain_choice()
            else:
                asyncio.create_task(self.start_day_vote())

    
    def voteKillCallback(self, choice):
        self.current_vote = None
        char, name = choice
        self.last_day_victim = char if char != SpecialVote.BLANK else None
        asyncio.create_task(self.day_end_vote())
    
    async def start_day_vote(self):
        self.state = GameState.STATE_DAY_VOTE
        
        await self.main_channel.send(f"Vous devez maintenant discuter (ici, ou en message privé) puis **voter pour déterminer si vous souhaitez tuer un membre d'équipage :ballot_box::skull:**. Vous pouvez voter blanc, si le vote blanc a la majorité personne n'est tué. En cas d'égalité, votre capitaine ({self.captain.getName()}) tranchera entre les options à égalité.")
        
        
        # Create the day vote
        self.current_vote = Vote(f"Vote pour exécuter an membre de l'équipage - Jour {self.current_day}",
            self.voteKillCallback,
            self.main_channel,
            options=[(ch, ch.getName()) for ch in self.alive_people()],
            voters=[ch.player for ch in self.alive_people()],
            tiebreaker=self.captain.player,
            blank_allowed=True,
            abstain_allowed=True)
            
        #Start !
        await self.current_vote.start()
        
        
    async def day_end_vote(self):
        self.state = GameState.STATE_DAY_EVENING
        await self.lock_main_channel()
        if self.last_day_victim:
            await self.main_channel.send(f":ballot_box::skull: Vous avez décidé de **tuer {self.last_day_victim.player.mention} !** Al est immédiatement exécutæ.")
            await self.logs_channel.send(f":ballot_box::skull: L'équipage a décidé de **tuer {self.last_day_victim.getName()} !** Al était : {self.last_day_victim.autopsy()}.")
            await self.last_day_victim.kill() # Do the deed
            await self.main_channel.send(f"**{self.last_day_victim.getName()}** était : {self.last_day_victim.autopsy()}")
        else:
            await self.main_channel.send(":ballot_box::skull: Vous avez décidé de **ne tuer personne !**")
            await self.logs_channel.send(":ballot_box::skull: L'équipage a décidé de **ne tuer personne**.")

        await self.check_endgame()
        
        if not self.state == GameState.STATE_ENDGAME:
            if self.last_day_victim and self.last_day_victim.isCaptain:
                await self.main_channel.send("**Vous avez exécuté votre Capitaine**, merci de patientez pendant qu'al nomme son successeur...")
                await self.init_captain_choice()
            else:
                asyncio.create_task(self.new_night()) # Start new night

    async def check_endgame(self):
        if(len(self.alive_mutants()) == 0):
            await self.endgame("astronauts")
        elif(len(self.alive_mutants()) == len(self.alive_people())):
            await self.endgame("mutants")
    
    
    async def endgame(self, victory=None):
        self.state = GameState.STATE_ENDGAME
        if victory == "mutants":
            await self.main_channel.send("**__=== FIN DU JEU - Victoire des mutanz :biohazard: ! ===__**")
            await self.logs_channel.send("**__=== FIN DU JEU - Victoire des mutanz :biohazard: ! ===__**")
            await self.main_channel.send("L'équipage a entièrement été éliminé ou muté par les mutanz. Le vaisseau est sous le contrôle de la Spore.")
        elif victory == "astronauts":
            await self.main_channel.send("**__=== FIN DU JEU - Victoire des sains :astronaut: ! ===__**")
            await self.logs_channel.send("**__=== FIN DU JEU - Victoire des sains :astronaut: ! ===__**")
            await self.main_channel.send("Touz les mutanz ont touz été tuæs ou soignæs, la menace de la Spore est éliminée.")
        else:
            await self.main_channel.send("**__=== FIN DU JEU - Partie terminée par l'organisataire :astronaut::biohazard: ! ===__**")
            await self.logs_channel.send("**__=== FIN DU JEU - Partie terminée par l'organisataire :astronaut::biohazard: ! ===__**")
        for p in self.players:
            await self.logs_channel.set_permissions(p, view_channel=True, send_messages=False, add_reactions=False) # Read only permission to the game log for everyone
            await self.main_channel.set_permissions(p, view_channel=True, send_messages=True, add_reactions=True) # Restore full permissions to main & dead channels to everyone
            await self.dead_channel.set_permissions(p, view_channel=True, send_messages=True, add_reactions=True) # Restore full permissions to main & dead channels to everyone
            await self.voice_channel.set_permissions(p, connect=True, speak=True) # Everyone can speak again !
        await self.main_channel.send(f"Vous pouvez continuer à échanger ici, quand vous avez fini, {self.organizer.mention} pourra faire `+endgame` pour supprimer les salons.")
