
import discord
from discord.ext import commands
from enum import Enum
import random

import asyncio

from sporzbot.game import Game, ChannelType


class GamesManager:
    def __init__(self):
        #games = list()
        self.game_per_channel_id = dict()
        self.game_per_user_id = dict()
    
    def create_game(self, channel: discord.TextChannel, organizer:discord.Member):
        if channel.id in self.game_per_channel_id:
            raise RuntimeError("A game is already being setup or run here !")
        
        if organizer.id in self.game_per_user_id:
            raise RuntimeError("{} is already in a game !".format(str(organizer)))
        
        game = Game(channel, organizer, self)
        
    def get_game_by_chanid(self, chanid: int) -> (Game, ChannelType):
        try:
            return self.game_per_channel_id[chanid]
        except KeyError:
            return (None, ChannelType.CHANNEL_NOT_IN_GAME)

    def get_game_by_uid(self, uid: int) -> Game:
        try:
            return self.game_per_user_id[uid]
        except KeyError:
            return (None, None)

