

import discord
import logging
from discord.ext import commands
import random
import typing
import asyncio, aiofiles

from sporzbot.game import GameState, ChannelType, Game, channel_types_desc
from sporzbot.gamesmanager import GamesManager
from sporzbot.vote import VoteState
from sporzbot.roles import Hacker, Spy


logging.basicConfig(level=logging.INFO)



# Intents
intents = discord.Intents.default()
intents.members = True
intents.messages = True
intents.message_content = True 

botprefix='+'

# Setup bot
bot = commands.Bot(command_prefix=botprefix, description="Bot permettant de maîtriser des parties de Sporz.", intents=intents)

gm = GamesManager()


class WrongChannelError(commands.CheckFailure):
    pass

class WrongPersonError(commands.CheckFailure):
    pass


# === Checks ===
def server_only():
    async def predicate(ctx):
        if ctx.guild is None:
            raise(WrongChannelError("Vous ne pouvez pas utiliser cette commande en message privé"))
        return True
    return commands.check(predicate)

def private_only():
    async def predicate(ctx):
        if ctx.guild is not None:
            raise(WrongChannelError("Vous devez envoyer cette commande en message privé"))
        return True
    return commands.check(predicate)

# Checks if the current channel is a game channel of the right type
# If CHANNEL_NOT_IN_GAME is requested, it will pass iif the current channel
# is a valid channel from a server, that is not part of any game.
# This will always fail if the command is invoked in DM
def in_game_channel(requested_type: ChannelType):
    async def predicate(ctx):
        if ctx.guild is None:
            raise(WrongChannelError("Vous ne pouvez pas utiliser cette commande en message privé"))
        game, chantype = gm.get_game_by_chanid(ctx.channel.id)
        if (requested_type != chantype):
            raise(WrongChannelError(f"Vous ne pouvez utiliser cette commande que dans un canal de type : {channel_types_desc[requested_type]}, mais vous êtes dans un canal de type {channel_types_desc[chantype]}"))
        return True
    return commands.check(predicate)

# Passes if the member that summoned the command is in a game
def in_game():
    async def predicate(ctx):
        game, _ = gm.get_game_by_uid(ctx.author.id)
        if game is None:
            raise(WrongPersonError("Vous ne pouvez pas utiliser cette commande car vous n'êtes pas dans une partie"))
        return True
    return commands.check(predicate)


# Passes if the member that summoned the command is in a game that is in the requested state
def in_game_state(requested_state: typing.Union[GameState, list[GameState]]):
    async def predicate(ctx):
        if not isinstance(requested_state, list): # Listify
            list_req = [requested_state]
        else:
            list_req = requested_state
        game, _ = gm.get_game_by_uid(ctx.author.id)
        if game is None:
            raise(WrongPersonError("Vous ne pouvez pas utiliser cette commande car vous n'êtes pas dans une partie"))
        if game.state not in list_req:
            raise(WrongPersonError("Vous ne pouvez pas utiliser cette commande à ce stade du jeu"))
        return True
    return commands.check(predicate)

# Passes if the member is in a game, that has a vote in progress, and is in the vote
def is_in_vote(required_state=VoteState.VOTE_IN_PROGRESS):
    async def predicate(ctx):
        game, _ = gm.get_game_by_uid(ctx.author.id)
        if game is None:
            raise(WrongPersonError("Vous ne pouvez pas utiliser cette commande car vous n'êtes pas dans une partie"))
        if game.current_vote is None or game.current_vote.state != required_state:
            raise(WrongPersonError("Aucun vote n'est en cours, ou ce n'est pas le bon moment pour utiliser cette commande."))
        if ctx.author not in game.current_vote.votes:
            raise(WrongPersonError("Vous ne faites pas partie du vote en cours"))
        return True
    return commands.check(predicate)


# Passes if the member that summoned the command is the organizer of the current game
def is_organizer():
    async def predicate(ctx):
        game, _ = gm.get_game_by_uid(ctx.author.id)
        if((game is None) or (game.organizer != ctx.author)):
            raise(WrongPersonError("Seulx l'organisataire de la partie en cours peut utiliser cette commande"))
        return True
    return commands.check(predicate)

# Passes if the member that summoned the command is NOT currently in a game
def not_in_game():
    async def predicate(ctx):
        game, _ = gm.get_game_by_uid(ctx.author.id)
        if game is not None:
            raise(WrongPersonError("Vous ne pouvez pas utiliser cette commande car vous êtes déjà dans une partie"))
        return True
    return commands.check(predicate)

# === Ready ===
@bot.event
async def on_ready():
    print('Logged in as')
    print(bot.user.name)
    print(bot.user.id)
    print('------')

# === Commands ===

@bot.group(help="Commandes liées au démarrage ou au paramétrage d'une partie", brief="Créer, démarrer, arrêter des parties")
async def game(ctx):
    if ctx.invoked_subcommand is None:
        await ctx.send('Commande invalide')

@game.command(help="Crée une nouvelle partie")
@in_game_channel(ChannelType.CHANNEL_NOT_IN_GAME)
async def new(ctx):
    gm.create_game(ctx.channel, ctx.author)
    await ctx.send(f"Démarrage de partie en cours, tapez `{botprefix}game join` pour rejoindre")

@game.command(brief="Rejoindre la partie",description="Permet de rejoindre une partie en cours de création")
@in_game_channel(ChannelType.CHANNEL_PRESETUP)
@not_in_game()
async def join(ctx):
    game, _ = gm.get_game_by_chanid(ctx.channel.id)
    game.add_player(ctx.author)
    await ctx.send(f"{ctx.author.display_name} a rejoint la partie; {len(game.players)} jouaires")


@game.command(help="Lance la partie avec les jouaires qui l'ont rejointe")
@in_game_channel(ChannelType.CHANNEL_PRESETUP)
@is_organizer()
async def start(ctx):
    game, _ = gm.get_game_by_chanid(ctx.channel.id)
    await game.start()


@game.command(brief="Annuler la partie", description="Met immédiatement fin à la partie et supprime tous les salons liés.")
@is_organizer()
async def abort(ctx):
    game, _ = gm.get_game_by_uid(ctx.author.id)
    await game.abort()


# Endgame
@game.command(brief="Terminer la partie", description="Termine le jeu sans déclarer explicitement de victoire, ou par la victoire d'un camp en donnant 'sains' ou 'mutants'.")
@is_organizer()
@in_game_state(GameState.STATE_DAY_VOTE)
async def stop(ctx, winner:typing.Optional[str]):
    game, _ = gm.get_game_by_uid(ctx.author.id)
    if(winner=="sains" or winner=="astronauts"):
        await game.endgame(victory="astronauts")
    elif(winner=="mutants"):
        await game.endgame(victory="mutants")
    else:
        await game.endgame(victory=None)





@game.command(help="Démarre la partie avec les paramètres en cours")
@in_game_channel(ChannelType.CHANNEL_MAIN)
@is_organizer()
@in_game_state(GameState.STATE_SETUP)
async def begin(ctx):
    game, _ = gm.get_game_by_uid(ctx.author.id)
    await game.begin()


# Forcing/debug commands

@game.group(help="Commandes de forçage et de débugging", hidden=True)
@is_organizer()
async def force(ctx):
    if ctx.invoked_subcommand is None:
        await ctx.send('Commande invalide')

@force.command(help="Force le déclenchement d'une phase du jeu", hidden=True, aliases=["state"])
@is_organizer()
async def phase(ctx, number:int):
    if number < 0 or number > 9:
        await ctx.send('Donnez un état entre 0 et 9')
    else:
        game, _ = gm.get_game_by_uid(ctx.author.id)
        await ctx.send(f":warning: Tentative de forçage de l'état #{number}")
        if(number == 0):
            game.state = GameState.STATE_PRESETUP
        elif(number == 1):
            await game.start()
        elif(number == 2):
            await game.begin()
        elif(number == 3):
            await game.new_night()
        elif(number == 4):
            await game.setup_phase1_turn()
        elif(number == 5):
            await game.setup_phase2_turn()
        elif(number == 6):
            await game.setup_end_night()
        elif(number == 7):
            await game.start_day_vote()
        elif(number == 8):
            await game.day_end_vote()
        elif(number == 9):
            await game.endgame()

@force.command(help="Force la fin du vote en cours", hidden=True)
@is_organizer()
async def endvote(ctx):
    game, _ = gm.get_game_by_uid(ctx.author.id)
    if not game.current_vote:
        await ctx.send('Aucun vote en cours')
    else:
        await ctx.send(":warning: Tentative de forçage de la fin du vote en cours")
        await game.current_vote.vote_finished()




# A voir : kick, unjoin


@bot.command(help="Modifie un paramètre de la partie")
@in_game_channel(ChannelType.CHANNEL_MAIN)
@is_organizer()
@in_game_state(GameState.STATE_SETUP)
async def set(ctx, paramname: str, value: int):
    game, _ = gm.get_game_by_uid(ctx.author.id)
    param = game.getparam(paramname)
    if param is None:
        await ctx.send(f"Paramètre {paramname} inconnu")
    else:
        await param.set(value, ctx.channel)

# Votes

@bot.command(help="Permet de voter à un vote en cours. Les valeurs autorisées sont le numéro d'une option, 'blank' si le vote blanc est autorisé ou 'pass' pour s'abstenir.", brief="Voter à un vote")
@is_in_vote()
@private_only()
async def vote(ctx, what: typing.Union[int, str]):
    game, _ = gm.get_game_by_uid(ctx.author.id)
    vote = game.current_vote
    await vote.vote(ctx.author, what)

@bot.command(help="Affiche des informations sur le vote")
@is_in_vote()
async def voteinfo(ctx):
    game, _ = gm.get_game_by_uid(ctx.author.id)
    vote = game.current_vote
    if ctx.guild is None:
        await vote.vote_info_private(ctx.author)
    else:
        await vote.vote_info_public()


@bot.command(help="Trancher une égalité lors d'un vote")
@is_in_vote(required_state=VoteState.VOTE_COUNTING_BREAK_TIE)
@in_game_channel(ChannelType.CHANNEL_MAIN)
async def choose(ctx, choice: int):
    game, _ = gm.get_game_by_uid(ctx.author.id)
    vote = game.current_vote
    if vote.tiebreaker != ctx.author:
        await ctx.send("Vous ne pouvez pas trancher ce vote")
    else:
        await vote.break_tie(choice)


@bot.command(help="Envoyer des rappels aux personnes qui n'ont pas encore voté")
@is_in_vote()
@is_organizer()
@in_game_channel(ChannelType.CHANNEL_MAIN)
async def voterecall(ctx):
    game, _ = gm.get_game_by_uid(ctx.author.id)
    await game.current_vote.voterecall()




# Mutants

@bot.command(brief="Tuer un membre d'équipage", description="Permet de proposer de tuer un membre d'équipage. Touz les mutanz doivent se concerter pour tuer la même personne, cela remplace la mutation.")
@in_game_channel(ChannelType.CHANNEL_MUTANT_CURRENT)
@in_game_state(GameState.STATE_NIGHT_MUTANT)
async def kill(ctx, choice: int):
    game, char = gm.get_game_by_uid(ctx.author.id)
    if not char.mutantMajorActionAvailable:
        await ctx.send("Vous ne pouvez pas faire cette action")
        return
    await char.action_kill_mutant(choice)
    await game.check_end_night_mutant()

@bot.command(brief="Muter un membre d'équipage", description="Permet de proposer de muter un membre d'équipage. Touz les mutanz doivent se concerter pour muter la même personne.")
@in_game_channel(ChannelType.CHANNEL_MUTANT_CURRENT)
@in_game_state(GameState.STATE_NIGHT_MUTANT)
async def mutate(ctx, choice: int):
    game, char = gm.get_game_by_uid(ctx.author.id)
    if not char.mutantMajorActionAvailable:
        await ctx.send("Vous ne pouvez pas faire cette action")
        return
    await char.action_mutate(choice)
    await game.check_end_night_mutant()

@bot.command(brief="Paralyser un membre d'équipage", description="Permet de proposer de paralyser un membre d'équipage. Touz les mutanz doivent se concerter pour paralyser la même personne. Cette action a lieu en plus de la mutation ou du meurtre.")
@in_game_channel(ChannelType.CHANNEL_MUTANT_CURRENT)
@in_game_state(GameState.STATE_NIGHT_MUTANT)
async def paralyze(ctx, choice: int):
    game, char = gm.get_game_by_uid(ctx.author.id)
    if not char.mutantMinorActionAvailable:
        await ctx.send("Vous ne pouvez pas faire cette action")
        return
    await char.action_paralyze(choice)
    await game.check_end_night_mutant()

#Doctors
@bot.command(brief="Tuer un membre d'équipage", description="Permet de proposer de tuer un membre d'équipage. Touz les médecins réveillæs doivent se concerter pour tuer la même personne. Cette action remplace tous les soins.")
@in_game_channel(ChannelType.CHANNEL_DOCTOR)
@in_game_state(GameState.STATE_NIGHT_DOCTOR)
async def drkill(ctx, choice: int):
    game, char = gm.get_game_by_uid(ctx.author.id)
    if not char.doctorActionAvailable:
        await ctx.send("Vous ne pouvez pas faire cette action")
        return
    await char.action_kill_doctor(choice)
    await game.check_end_night_doctor()

@bot.command(brief="Soigner un membre d'équipage", description="Permet de proposer de soigner un membre d'équipage. Chaque médecin réveillæ peut soigner une personne, l'action est exécutée dès que tout le monde a dit qui al soignait.")
@in_game_channel(ChannelType.CHANNEL_DOCTOR)
@in_game_state(GameState.STATE_NIGHT_DOCTOR)
async def cure(ctx, choice: int):
    game, char = gm.get_game_by_uid(ctx.author.id)
    if not char.doctorActionAvailable:
        await ctx.send("Vous ne pouvez pas faire cette action")
        return
    await char.action_cure(choice)
    await game.check_end_night_doctor()


#Phase 1 inspection roles
@bot.command(brief="Inspecter un membre d'équipage", description="Permet à an psychologue ou an génétician de connaître l'état de mutation ou le génome d'une personne.")
@private_only()
@in_game_state(GameState.STATE_NIGHT_ROLE1)
async def inspect(ctx, choice: int):
    game, char = gm.get_game_by_uid(ctx.author.id)
    if not char.phase1ActionAvailable:
        await ctx.send("Vous ne pouvez pas faire cette action")
        return
    await char.inspect(choice)
    await game.check_end_phase1()

#Phase 2 roles
@bot.command(brief="Hacker un rôle", description="Permet à an hacker de pirater les données du psychologue, génétician ou informatician. On ne peut pas pirater le même rôle deux fois de suite.")
@private_only()
@in_game_state(GameState.STATE_NIGHT_ROLE2)
async def hack(ctx, choice: str):
    game, char = gm.get_game_by_uid(ctx.author.id)
    if not char.phase2ActionAvailable:
        await ctx.send("Vous ne pouvez pas faire cette action")
        return
    if not isinstance(char, Hacker):
        await ctx.send("Seulx an Hacker peut faire cela")
        return
    await char.hack(choice)
    await game.check_end_phase2()

#Phase 2 roles
@bot.command(brief="Espionner un membre d'équipage", description="Permet à an espionx de surveiller un membre d'équipage, pour découvrir tout ce qu'al lui est arrivæ cette nuit.")
@private_only()
@in_game_state(GameState.STATE_NIGHT_ROLE2)
async def spy(ctx, choice: int):
    game, char = gm.get_game_by_uid(ctx.author.id)
    if not char.phase2ActionAvailable:
        await ctx.send("Vous ne pouvez pas faire cette action")
        return
    if not isinstance(char, Spy):
        await ctx.send("Seulx an Espion peut faire cela")
        return
    await char.spy(choice)
    await game.check_end_phase2()


#Nominate new captain
@bot.command(brief="Nommer son successeur", description="Permet à an Capitaine mort de nommer son successeur.")
@private_only()
@in_game_state([GameState.STATE_DAY_MORNING,GameState.STATE_DAY_EVENING]) # The two moments during which this can happen
async def captain(ctx, choice: int):
    game, char = gm.get_game_by_uid(ctx.author.id)
    if not (char.isDead and char.isCaptain):
        await ctx.send("Vous ne pouvez pas faire cette action")
        return
    await game.nominate_captain(choice)


# Endgame
@bot.command(brief="Clore la partie", description="Permet de supprimer le jeu, et les salons liés.")
@is_organizer()
@in_game_state(GameState.STATE_ENDGAME)
async def endgame(ctx):
    game, _ = gm.get_game_by_uid(ctx.author.id)
    await game.abort()

# Rules
@bot.command(brief="Afficher les règles", aliases=["regles","règles","rule"], description="Envoie les règles de Sporz en MP à qui les demande.")
async def rules(ctx):
    if ctx.guild is not None:
        await ctx.send("OK, je vous infodumpe en MP avec les règles (c'est un peu long) :biohazard::alien:")
    dest = ctx.author
    i = 1;
    while True:
        try:
            async with aiofiles.open(f"rules/fr/sporz{i}.md","r") as fich_regles:
                data = await fich_regles.read()
            await asyncio.sleep(1) # Do not flood
            await dest.send(data)
            i += 1
        except OSError:
            break



#Error handling
@bot.event
async def on_command_error(ctx, error):
    await ctx.send(error)



