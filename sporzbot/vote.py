

import discord
import typing
import random
from enum import Enum 

class VoteState(Enum):
    VOTE_NOT_STARTED=0
    VOTE_IN_PROGRESS=1
    VOTE_COUNTING_BREAK_TIE=2
    VOTE_OVER=3

class SpecialVote(Enum):
    BLANK=0
    ABSTAIN=1

class Vote:
    def __init__(self, vote_name: str, result_callback, public_channel: discord.TextChannel,  options,  voters: list[discord.Member], tiebreaker : typing.Optional[discord.Member], blank_allowed: bool=True, abstain_allowed: bool = True):
        self.name = vote_name # Name of the vote
        self.callback = result_callback # Callback to call when the vote is over
        self.public_channel = public_channel # Public channel to list options and announce results
        self.options = options # Vote options : a list of (Object, str) with object to return and description
        self.votes = {
            voter: None
            for voter in voters
        }
        self.tied_winners = None
        self.winner = None
        self.tiebreaker = tiebreaker # Who to ask to break the tie ? (If None, will be selected randomly)
        self.abstain_allowed = abstain_allowed
        self.blank_allowed = blank_allowed
        self.state = VoteState.VOTE_NOT_STARTED
    
    def n_votes(self):
        return len(list(filter(lambda x: x is not None, self.votes.values())))
    
    def n_missing_votes(self):
        return len(list(filter(lambda x: x is None, self.votes.values())))
    
    def n_voters(self):
        return len(self.votes)
    
    def missing_votes(self):
        return list(filter(lambda v: self.votes[v] is None, self.votes.keys()))
    
    def getOptions(self):
        optionTexts = [f"`+vote {i+1}` : {name}" for (i,(_,name)) in enumerate(self.options)]
        if self.blank_allowed:
            optionTexts.append("`+vote blank` : Vote blanc (personne)")
        if self.abstain_allowed:
            optionTexts.append("`+vote pass` : Abstention")
        return "\n".join(optionTexts)
    
    async def vote_info_public(self):
        await self.public_channel.send(f"**Vote en cours :ballot_box: :** {self.name}\n__Options :__")
        await self.public_channel.send(self.getOptions())
        await self.public_channel.send(f"**Merci aux participanz de voter en message privé.** Pour le moment, {self.n_votes()} votes sur {self.n_voters()} ont été reçus.\nTapez `+voteinfo` pour consulter à nouveau ces informations.")
    
    async def vote_info_private(self, who: discord.Member):
        if who not in self.votes:
            await who.send("Vous ne faites pas partie de ce vote.")
            return
        myvote = self.votes[who]
        await who.send(f"**Vote en cours :ballot_box: :** {self.name}\n__Options :__")
        await who.send(self.getOptions())
        if myvote is None:
            await who.send("**Vous n'avez pas encore voté.** Merci de le faire quand vous serez décidæ !")
        else:
            await who.send(f"Votre vote actuel est : **{myvote[1]}** Vous pouvez le changer jusqu'à ce que le vote soit terminé.")
        await who.send(f"Pour le moment, {self.n_votes()} votes sur {self.n_voters()} ont été reçus.\nTapez `+voteinfo` pour consulter à nouveau ces informations.")

    async def voterecall(self):
        for v in self.missing_votes():
            await v.send(f"**:warning::ballot_box: N'oubliez pas de participer au vote en cours !** ({self.n_votes()} votes sur {self.n_voters()} ont été reçus.")
        await self.public_channel.send(f"Rappels de vote envoyés à {self.n_missing_votes()}/{self.n_votes()} personnes.")



    async def start(self):
        if self.state != VoteState.VOTE_NOT_STARTED:
            raise RuntimeError("Ce vote a déjà commencé ou déjà eu lieu")
        await self.vote_info_public()
        for voter in self.votes.keys():
            await self.vote_info_private(voter)
        self.state = VoteState.VOTE_IN_PROGRESS
    
    async def vote(self, who:discord.User, what:typing.Union[int, str]):
        if who not in self.votes:
            raise RuntimeError("Wrong voter")
        if isinstance(what,int):
            if what < 1 or what > len(self.options):
                await who.send("Merci d'entrer un vote valide")
                return
            else:
                self.votes[who] = self.options[what-1]
        else:
            if what == "blank" and self.blank_allowed:
                self.votes[who] = (SpecialVote.BLANK, "Vote blanc")
            elif what == "pass" and self.abstain_allowed:
                self.votes[who] = (SpecialVote.ABSTAIN, "Abstention")
            else:
                await who.send(f"Vote {what} invalide.")
                return
        await who.send(f"Vote enregistré : **{self.votes[who][1]}**. ({self.n_votes()}/{self.n_voters()} votes reçus au total)")
        if self.n_votes() == self.n_voters(): # Vote is over !
            self.state = VoteState.VOTE_COUNTING_BREAK_TIE
            await self.vote_finished()
    
    async def vote_finished(self):
        await self.public_channel.send(f"**Vote '{self.name}' terminé !**")
        resultslist, abstain = self.count_results()
        await self.print_results(resultslist, abstain)
        winners = self.get_winners(resultslist)
        if winners is None: # No winner, because everybody passed -_-
            if self.tiebreaker is not None: # If there is a tiebreaker, just make them choose !
                self.tied_winners = list(self.options) # All options are open
                if self.blank_allowed: # If blank is allowed, allow the tiebreaker to make blank win
                    self.tied_winners.append((SpecialVote.BLANK, "Vote blanc"))
                await self.public_channel.send(f"Tout le monde s'est abstenu :expressionless: C'est {self.tiebreaker.display_name} qui  choisira le résultat.")
            else: # Everybody passed, no tiebreaker
                if self.blank_allowed: # If blank is allowed, just declare blank winner
                    self.winner = (SpecialVote.BLANK, "Vote blanc")
                    await self.public_channel.send("Tout le monde s'est abstenu :expressionless: Le vote blanc est déclaré vainqueur.")
                else: # Blank is not allowed -> choose a random option
                    self.tied_winners = list(self.options) # All options are open
                    await self.public_channel.send("Tout le monde s'est abstenu :expressionless: Le résultat sera déterminé au hasard.")
        else: # We have at least one winner
            if len(winners) == 1: # One clear winner
                await self.public_channel.send(f"Résultat du vote : **{winners[0][1]}** !")
                self.winner = winners[0]
            else:
                winners_str = ", ".join([w[1] for w in winners])
                await self.public_channel.send(f"Résultat du vote : **égalité entre {winners_str}**")
                self.tied_winners =  list(winners)
        # We have one or more winners
        if self.winner is not None: # One clear winner
            self.finish()
        else:
            if self.tiebreaker is None: # No tiebreaker
                await self.public_channel.send("Sélection automatique d'un résultat entre les proposition à égalité :game_die: ...")
                self.winner = random.choice(self.tied_winners)
                await self.public_channel.send(f"Résultat final : **{self.winner[1]}** !")
                self.finish()
            else:
                await self.public_channel.send(f"C'est à {self.tiebreaker.display_name} de choisir entre les options suivantes :")
                await self.public_channel.send("\n".join([f"- `+choose {i+1}` : {desc}" for (i, (_, desc)) in enumerate(self.tied_winners)]))
    
    async def break_tie(self,choice):
        if choice < 1 or choice > len(self.tied_winners):
            await self.public_channel.send(f"Choix incorrect, entrez un nombre entre 1 et {len(self.tied_winners)}")
        else:
            self.winner = self.tied_winners[choice-1]
            await self.public_channel.send(f"Résultat final : **{self.winner[1]}** !")
            self.finish()
    
    
    def finish(self):
        self.state = VoteState.VOTE_OVER
        self.callback(self.winner)
        
    def count_results(self):
        results = dict()
        abstain = 0
        for votevalue in self.votes.values():
            if votevalue is None: # Should only happen if the vote was force ended
                abstain += 1 # Count as abstain
                continue
            (vote, desc) = votevalue
            if vote == SpecialVote.ABSTAIN: # Explicit abstain
                abstain += 1
                continue
            if vote not in results:
                results[vote] = [1, desc]
            else:
                results[vote][0] += 1
        resultslist = list(results.items())
        resultslist.sort(key=lambda x:-x[1][0])
        return resultslist, abstain
    
    async def print_results(self, resultslist, abstain):
        await self.public_channel.send("__Résultats du vote :__")
        resultsitems = [f"- {r[1][1]} : {r[1][0]} votes" for r in resultslist]
        if abstain > 0:
            resultsitems.append(f"{abstain} abstentions")
        resultstext = "\n".join(resultsitems)
        await self.public_channel.send(resultstext)
    
    def get_winners(self, resultslist):
        if len(resultslist) == 0: # Everybody abstained !
            return None
        else:
            winners = [(resultslist[0][0], resultslist[0][1][1])]
            winningscore = resultslist[0][1][0]
            for i in range(1, len(resultslist)):
                if resultslist[i][1][0] == winningscore:
                    winners.append((resultslist[i][0], resultslist[i][1][1])) # Tied winners
                else:
                    break
            return winners
    
    
    
