import discord


MIN_PLAYERS=4 # Bare minimum number of players
MIN_PLAYERS_WARNING=7 # Recommended minimum number of players
MAX_PLAYERS_WARNING=15 # Recommended max number of players




def get_n_doctors(n_players):
    return 2 # More or less than 2 doctors is usually too unbalancing, whatever the number of players
    
def get_n_mutants(n_players):
    if(n_players < 12):
        return 1
    else:
        return 2 # Only put 2 mutants if there are a lot of players

def get_n_psy(n_players):
    if(n_players < 5):
        return 0
    else:
        return 1

def get_n_gen(n_players):
    if(n_players < 6):
        return 0
    else:
        return 1

def get_n_computer(n_players):
    if(n_players < 7):
        return 0
    else:
        return 1

def get_n_hacker(n_players):
    if(n_players < 8):
        return 0
    else:
        return 1

def get_n_spy(n_players):
    if(n_players < 10):
        return 0
    else:
        return 1


def get_n_extra_host(n_players):
    if(n_players < 5):
        return 0
    else:
        return 1


def get_n_resist(n_players):
    return 1

def get_doctor_status(n_players):
    return 0 # Could be set to 1 for n_players <8


class GameParameter:
    def __init__(self, name, default, vmin, vmax, description):
        self.name = name
        self.value = default
        self.description = description
        self.min = vmin
        self.max = vmax
    

    def get(self):
        return f"`{self.name}` - {self.description} : **{self.value}**"

    async def print(self, channel: discord.TextChannel):
        await channel.send(self.get())
    

    async def set(self, value: int, channel: discord.TextChannel):
        if(self.min != None and value < self.min):
            await channel.send(f"`{self.name}` doit valoir au moins {self.min}")
        elif(self.max != None and value > self.max):
            await channel.send(f"`{self.name}` doit valoir au plus {self.max}")
        else:
            self.value = value
            await channel.send(f"`{self.name}` fixé à {self.value}")


def gen_parameters(n_players: int):
    params = dict()
    params["n_mutants"] = GameParameter("n_mutants", get_n_mutants(n_players), 1, None, "Nombre de mutanz de base")
    params["n_doctors"] = GameParameter("n_doctors", get_n_doctors(n_players), 1, None, "Nombre de médecins")
    params["n_psy"] = GameParameter("n_psy", get_n_psy(n_players), 0, None, "Nombre de psychologues")
    params["n_gen"] = GameParameter("n_gen", get_n_gen(n_players), 0, None, "Nombre de généticians")
    params["n_computer"] = GameParameter("n_computer", get_n_computer(n_players), 0, None, "Nombre d'informaticians")
    params["n_hacker"] = GameParameter("n_hacker", get_n_hacker(n_players), 0, None, "Nombre de hackers")
    params["n_spy"] = GameParameter("n_spy", get_n_spy(n_players), 0, None, "Nombre d'espionz")
    params["n_extra_hosts"] = GameParameter("n_extra_hosts", get_n_extra_host(n_players), 0, None, "Nombre de personnages hôtes (en plus du ou des mutanz de base)")
    params["n_resist"] = GameParameter("n_resist", get_n_resist(n_players), 0, None, "Nombre de personnages résistants")
    params["doctor_status_known"] = GameParameter("doctor_status_known", get_doctor_status(n_players), 0, 1, "Les médecins connaissent-als l'état de mutation de leur collègue qui ne se réveille pas ? (1=oui, 0=non)")
    return params
    


