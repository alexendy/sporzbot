__**:biohazard: SPORZ :biohazard: **__

*Dans un futur lointain, un immense vaisseau spatial s'élance dans l'espace, emportant vers un monde détecté comme habitable des milliers de colons en hypersommeil. Le vaisseau est largement automatisé; seul un équipage d'une dizaine de personnes est présent, et il n'est réveillé que pour superviser le débarquement, ou en cas de crise.*

*C'est pour cette dernière éventualité que l'Ordinateur de Bord vous a réveillæ : une crise. Dès que vous vous extrayez du cocon de fugue cryotechnique, vous entendez l'alarme. ":red_circle: ALERTE ! AGENT BIOLOGIQUE MUTAGENE DETECTE ! EQUIPAGE CONTAMINE ! PROTOCOLE DE CONFINEMENT ACTIF !"*

*Avec vos collègues, vous prenez conscience de la gravité de la situation. L'an d'entre vous a été contaminæ par une **spore mutante**, un terrible agent pathogène presque indétectable, qui pousse ses portaires à contaminer toujours plus de personnes... Vous vous observez les ans les autres. Qui est contaminæ ? Le découvrir avant l'arrivée du vaisseau et contenir la contamination est indispensable, ou c'est tout un monde qui sera perdu à la Spore...*


__**Principe général**__

Sporz est un jeu de bluff, de déduction, de stratégie et de fourberie pour 7 à 15 jouaires. Il se joue en principe physiquement mais peut être joué sur discord. Les mécaniques de base du jeu sont proches de celle du jeu Loups-Garous de Thiercelieux, et seront familières à ses jouaires.

Durant la partie, les jouaires sont divisés en deux camps : les **mutants** et les **sains** (ou "astronautes"), comme les loups-garous et les villageois à Loups-Garous. Mais contrairement à Loups-Garous, __ces deux camps ne sont pas statiques__ : un mutant peut être soigné, et un sain peut être muté. Vos ennemis d'hier peuvent devenir vos alliés et vice versa, et c'est ce qui fait tout l'intérêt et la subtilité du jeu.

Le but de chaque camp est l'élimination totale de l'autre : les mutants doivent muter ou tuer tous les sains, et les sains doivent soigner ou tuer tous les mutants.

Le jeu est constitué d'une alternance de jours et de nuits. Pendant le jour, l'équipage discute (collectivement, en petits groupes ou individuellement) et décide - ou non - d'exécuter un des leurs. Pendant la nuit, les mutants ont l'occasion de répandre la spore, et les membres de l'équipage dotés de rôles spéciaux peuvent les jouer, ce qui leur donne l'occasion de soigner la spore ou de découvrir certaines informations.

__**Personnages**__

Un personnage est défini par trois caractéristiques :
- Son **état** : sain ou mutant. Il est suceptible de varier pendant la partie suite aux mutations ou aux soins.
- Son **génome** : tout le monde n'est pas égal face à la spore. Votre génétique vous range dans l'une des trois catégories suivantes :
  * génome **résistant** : vous avez une immunité naturelle à la spore, et ne pouvez jamais être mutæ !
  * génome **hôte** : votre génétique vous empêche de combattre la spore. Vous n'êtes pas (forcément) mutanx initialement, mais si vous le devenez, vous ne pouvez jamais être soignæ !
  * génome **normal** : vous pouvez être mutæ ou soignæ normalement
On ne connaît généralement pas son propre génôme en début de partie. Le mutant de base est hôte, et al existe un autre hôte (sain au départ) et un résistant dans le reste de l'équipage.
- Son **rôle** : certaines personnes ont un rôle qui leur donne des capacités ou propriétés supplémentaires, détaillé ci-dessous:

Quand un personnage meurt une autopsie est pratiquée et son état, son rôle et son génome sont annoncés publiquement par l'Ordinateur de Bord (immédiatement de jour, au petit matin si la mort à lieu la nuit).

**:astronaut::rocket: Les astronautes**

Les astronautes sont des membres de l'équipage sans rôle particulier. Toux jouaire qui n'a pas un autre rôle est astronaute.

**:biohazard::alien: Le mutant de base**

Le mutant de base est la personne contaminée initialement, qui va devoir répandre la spore dans tout le vaisseau. C'est une position importante : l'avenir de la spore repose sur vos seules épaules, du moins initialement !

Le mutant de base est toujours de génome hôte. Les sains doivent donc absolument le trouver et le tuer pour se débarrasser de la spore.

"Mutant de base" n'est pas officiellement un rôle : si le mutant de base meurt, l'autopsie indique simplement "astronaute, mutant, de génome hôte".

*Il n'y a généralement qu'un seul mutant de base, sauf à plus de dix jouaires expérimentæs.*


**:syringe::medical_symbol:  Les médecins**

Les médecins sont un rôle vital pour le camp sains : eux seuls sont capables de soigner la spore, et de ramener des mutants (non-hôtes) dans le camp des sains. Au nombre de deux, chaque médecin réveillé peut soigner une personne par nuit. Si les mutants découvrent l'identité des médecins, les chances de victoire des sains sont généralement serieusement compromises.

Les médecins sont toujours de génome normal. Dissimuler leur identité aux yeux des mutants doit être une priorité pour eux-mêmes et pour les sains, et la découvrir est un objectif des mutants.

*Il y a deux médecins, qui travaillent ensemble et se connaissent initialement.*


**:brain::notebook_with_decorative_cover:  Lea psychologue**

La spore est presque indétectable... mais presque n'est pas complètement. Elle affecte légèrement le comportement des personnes contaminées. Ce changement est trop subtil pour être détecté par la plupart des gens, mais il n'échappe pas à l'oeil d'an psychologue bien formé.

Une fois par nuit, le psychologue peut analyser le comportement d'une personne, et découvrir si cette personne est mutante ou saine.

*Il y a an psychologue par équipage*


**:scientist::dna: Lea génétician**

Lea génétician est capable d'analyser le génome de l'équipage pour connaître sa résistance à la spore.

Une fois par nuit, lea génétician peut analyser le génome d'une personne, et découvrir si cette personne est normale, résistante ou hôte. Al peut si al le souhaite s'auto-inspecter.a
*Al y a an génétician par équipage*


**:computer::zero::one: L'informaticien**

L'informatician est capable, par de savants calculs statistiques, d'estimer le nombre de mutants à bord.

Une fois par nuit, l'Ordinateur de Bord indique à l'informatician le nombre de mutants dans l'équipage.

*Al y a an informatician par équipage*

**:desktop::fire: Lea hacker** 

Lea hacker peut aller s'introduire dans les ordinateurs des autres astronautes et consulter les fichiers confidentiels

Une fois par nuit, lea hacker se réveille après lea psychologue, lea généticien et l'informatician et décide de pirater les données d'un de ces trois rôles. Al ne peut pas pirater deux nuits de suite le même rôle. L'Ordinateur de Bord lui donne alors la même information qu'a eu ce rôle cette nuit : le nombre de mutants pour l'informatician, et la personne inspectée et le résultat de l'inspection pour lea psychologue ou lea génétician.

Le hacker pirate bien un __rôle__ (par exemple "psychologue") et non une personne ; il n'a pas besoin de savoir quæl jouaire a le rôle en question, et ne l'apprend pas. Si, pour une raison ou une autre, le rôle n'a eu aucune information ce tour ci, le hacker n'a rien non plus.

*Al y a généralement an hacker par équipage de 8 personnes ou plus*

**:detective::eye: L'espion**

L'espion peut surveiller une personne et apprendre tout ce qui lui est arrivé pendant une nuit.

L'espion se réveille en dernier et désigne une personne, sur laquelle il apprend les informations suivantes (pour la nuit passée) :
* A-t-elle été paralysée ?
* A-t-elle été mutée ?
* A-t-elle été soignée ?
* A-t-elle été inspectée par le psychologue ?
* A-t-elle été inspectée par le généticien ?

L'espion peut s'espionner lui-même. Il ne connaît pas le résultat, ni l'auteur des actions; juste si elles ont eu lieu.

*Al y a généralement un espion par équipage de 10 personnes ou plus*

**::supervillain::smiling_imp: Le traître**

Personne ne sait ce qui se passe dans la tête du traître, mais en tout cas il a décidé d'aider les mutants à répandre la spore !

Le traître est sain (initialement du moins) mais il joue toujours dans le camp des mutants, quel que soit son état de mutation. Au niveau mécanique il fonctionne exactement comme un simple astronaute; en particulier il ne connaît pas les mutants et ne se réveille pas avec eux (sauf s'il a lui-même été muté). Il gagne simplement si les mutants gagnent, et s'y emploie donc.

L'identité du traître est annoncée à l'autopsie s'il est tué.

*On peut ajouter un traître à partir de 9-10 jouaires, pour corser un peu la partie pour les sains sans pour autant rajouter un second mutant de base.*

__**Déroulement du jeu :clock1: **__

Au départ, l'Ordinateur de Bord tire au sort les rôles et les génomes, et communique à chaque personne son rôle. Il communique aussi à chaque médecin l'identité de son collègue et crée une discussion commune avec eux.

**:ballot_box: Election du chef**

La première chose à faire dans la partie est d'élire un chef (ou capitaine). Les personnes candidates se déclarent et peuvent faire un rapide discours de campagne, puis réalise un vote rapide avec des emojis (c'est un vote à main levée IRL). En cas d'égalité un second tour peut être organisé.

Le chef dispose du pouvoir de trancher les votes en cas d'égalité. Si le chef meurt, il nomme son successeur au moment de son autopsie.

Après l'élection du chef, on enchaîne immédiatement sur la première nuit.

**:crescent_moon::night_with_stars: La nuit**

Personne ne parle pendant la nuit, sauf cas prévus par les règles. La nuit se déroule comme suit :

__:biohazard: Tour des mutants :__
Les mutants se réveillent : l'Ordinateur de Bord crée une discussion avec tous les mutants du tour, qui peuvent se concerter par écrit pour décider quoi faire. Les mutants font deux actions par nuit : *paralyser* et *muter* une personne. Quel que soit le nombre de mutants présents, on paralyse une seule personne et on mute une seule personne. 
* Une personne paralysée ne joue pas son rôle pour cette nuit : elle ne se réveille pas à son tour, et ne peut faire de soin si elle est médecin ou obtenir des informations, etc. Sur un simple astronaute la paralysie n'a aucun effet, mais la personne sait quand même qu'elle a été paralysée.
* Une personne mutée rejoint le camp des mutants après le tour des mutants en cours. Elle ne sait pas qui l'a mutée. Exception : si la personne mutée est de génome résistant, elle reste saine - mais elle apprend que les mutants ont tenté de la muter et que cela a échoué. Les mutants ne sont pas au courant de cet échec.

On peut muter et paralyser la même personne, mais c'est rare. Les mutants peuvent se paralyser eux-mêmes. Les mutants doivent décider autant que faire se peut à l'unanimité et au consensus.

*Action alternative : Tuer.* A la place de muter, les mutants peuvent choisir de tuer une personne. L'Ordinateur de Bord annonce publiquement et immédiatement que la personne a été tuée par les mutants, et elle ne peut plus être ciblée par des actions. L'autopsie n'a lieu qu'au petit matin.


__:syringe: Tour des médecins :__
Les méecins se réveillent, et peuvent discuter dans leur canal dédié. __Un médecin paralysé ou muté ne se réveille pas__, et ne peut donc pas discuter avec son collègue, ou soigner qui que ce soit.

Chaque médecin réveillé peut soigner une personne. Si un médecin ne se réveille pas, l'autre médecin ne peut que le constater et ne sait a priori pas si son collègue a été muté ou paralysé ; dans le doute il va donc généralement utiliser son soin pour le soigner (un médecin mutant risquant de dénoncer son collègue, ce qui est une catastrophe pour les sains).

Une personne mutante qui reçoit un soin redevient saine et joue immédiatement dans le camp des sains. Exception : si la personne est hôte, elle reste mutante - elle apprend qu'elle a reçu un soin, qui a échoué à cause de son génome. Les médecins ne sont pas au courant de cet échec. Un soin n'a aucun effet sur une personne saine (mais elle sait qu'elle a reçu un soin).

*Action alternative : Tuer.* A la place de soigner, l'ensemble des médecins réveillés peuvent se concerter pour choisir de tuer une seule personne. Le meurtre remplace __tous__ les soins du tour, peu importe qu'al y ait un ou deux médecins réveillés. L'Ordinateur de Bord annonce publiquement et immédiatement que la personne a été tuée par les médecins, et elle ne peut plus être ciblée par des actions. L'autopsie n'a lieu qu'au petit matin.

__:brain::dna::computer: Tour du psychologue, du généticien et de l'informaticien :__

Ces trois rôle se réveillent tour à tour (l'ordre importe peu), sauf si als ont été paralysæs, et obtiennent de l'Ordinateur de Bord leur information. Un rôle muté continue de recevoir des informations.

__:desktop::detective: Tour du hacker et/ou de l'espion (s'ils sont présents) :__

Ces deux rôles se réveillent après les trois précédents, sauf si als ont été paralysæs, et obtiennent de l'Ordinateur de Bord leur information. Un rôle muté continue de recevoir des informations.

**:sun_with_face::city_sunset: Le jour**

Au petit matin, si des gens sont morts pendant la nuit, l'autopsie est pratiquée et leur rôle, état et génome est annoncé publiquement par l'Ordinateur de Bord. Si le chef est mort de cette façon, il nomme immédiatement un successeur.

Pendant la journée, l'équipage discute librement à sa guise, en public, en petits groupes, en privé... en texte ou en vocal. (Merci d'inclure l'Ordinateur de Bord dans les messages privés.) Le but est d'échanger les informations avec son camp, de tenter d'identifier les mutants, et de décider de son vote, qui décidera si une personne sera exécutée par l'équipage aujourd'hui.

Le vote se fait à bulletin secret par message à l'Ordinateur de Bord. Trois possibilité existent :
* Voter contre an membre de l'équipage : s'il a la majorité, il sera tué
* Voter blanc, c'est à dire voter pour ne tuer personne : si blanc a la majorité, personne n'est tué
* S'abstenir et ne pas prendre part au vote

Dès que tout le monde a voté, l'Ordinateur comptabilise les votes et publie les résultats. En cas d'égalité, le chef tranche entre les options ayant récolté le plus de voix.

Si quelqu'an a été tuæ, son autopsie est publiée par l'Ordinateur de Bord. Puis, la nuit suivante tombe __immédiatement__ (pas d'autres discussions).

__**Règles de fair play**__

Sporz est conçu pour le jeu physique, jouer sur Discord demande quelques adaptations. En particulier :
- Il est interdit de discuter la nuit, sauf:
* Avec l'Ordinateur de Bord
* Les mutants entre eux pendant leur phase. (L'Ordinateur de Bord créera une nouvelle conversation à chaque tour.)
* Les médecins (réveillés) entre eux pendant leur phase. (Une conversation sera crée à cette fin par l'Ordinateur de Bord en début de partie.)
- Les morts sont priés de reposer en paix et de ne pas discuter avec les vivants ! Une conversation des morts sera crée par l'Ordinateur de Bord pour discuter entre elleux et commenter la partie.
- Les copier-coller de conversations d'autres personnes (que cela soit sous forme de texte ou d'image) sont interdits, car ils rendent le bluff impossible. Les échanges sur Discord même textuels simulent des conversations __orales__. Vous pouvez au plus citer quelques mots, comme al serait possible de le faire à l'oral.
- C'est important pour que le jeu marche bien de jouer **à fond** pour le camp dont on fait partie à un instant T. Si vous êtes sain, vous voulez absolument éliminer la spore, en soignant ou si nécessaire en tuant tous les mutants. A l'instant où vous êtes mutæ, vous voulez absolument la victoire des mutants, et vos anciens amis les sains doivent être mutæs ou éliminæs. Si vous êtes soignæ, vous trahirez sans aucun problème vos anciens compagnons mutants. Et chaque membre de l'équipage serait prêt à sacrifier sa vie pour faire gagner son camp.


**Conditions de victoire**
Elles sont simples :
- Si al n'y a plus aucun mutant à bord, les astronautes ont gagné. 
- Si tous les personnages en vie sont mutants, les mutants ont gagné.

Al est courant que les sains concèdent la partie si la situation est trop désespérée pour elleux, en particulier si les deux médecins sont morts ou mutés.

Les personnages "gagnent" et "perdent" avec le camp dont als font partie à la fin de la partie. (Le traître gagne et perd avec les mutants quel que soit son état de mutation.) Un personnage mort gagne ou perd avec le camp dont il faisait partie à sa mort.



- Vous êtes 


