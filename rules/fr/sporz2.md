_
__**Principe général**__

Sporz est un jeu de bluff, de déduction, de stratégie et de fourberie pour 7 à 15 jouaires. Il se joue en principe physiquement mais peut être joué sur discord. Les mécaniques de base du jeu sont proches de celle du jeu Loups-Garous de Thiercelieux, et seront familières à ses jouaires.

Durant la partie, les jouaires sont divisés en deux camps : les **mutants** et les **sains** (ou "astronautes"), comme les loups-garous et les villageois à Loups-Garous. Mais contrairement à Loups-Garous, __ces deux camps ne sont pas statiques__ : un mutant peut être soigné, et un sain peut être muté. Vos ennemis d'hier peuvent devenir vos alliés et vice versa, et c'est ce qui fait tout l'intérêt et la subtilité du jeu.

Le but de chaque camp est l'élimination totale de l'autre : les mutants doivent muter ou tuer tous les sains, et les sains doivent soigner ou tuer tous les mutants.

Le jeu est constitué d'une alternance de jours et de nuits. Pendant le jour, l'équipage discute (collectivement, en petits groupes ou individuellement) et décide - ou non - d'exécuter un des leurs. Pendant la nuit, les mutants ont l'occasion de répandre la spore, et les membres de l'équipage dotés de rôles spéciaux peuvent les jouer, ce qui leur donne l'occasion de soigner la spore ou de découvrir certaines informations.
