_
__**Règles de fair play**__

Sporz est conçu pour le jeu physique, jouer sur Discord demande quelques adaptations. En particulier :
- Il est interdit de discuter la nuit, sauf:
  * Avec l'Ordinateur de Bord
  * Les mutants entre eux pendant leur phase. (L'Ordinateur de Bord créera une nouvelle conversation à chaque tour.)
  * Les médecins (réveillés) entre eux pendant leur phase. (Une conversation sera crée à cette fin par l'Ordinateur de Bord en début de partie.)
- Les morts sont priés de reposer en paix et de ne pas discuter avec les vivants ! Une conversation des morts sera crée par l'Ordinateur de Bord pour discuter entre elleux et commenter la partie.
- Les copier-coller de conversations d'autres personnes (que cela soit sous forme de texte ou d'image) sont interdits, car ils rendent le bluff impossible. Les échanges sur Discord même textuels simulent des conversations __orales__. Vous pouvez au plus citer quelques mots, comme al serait possible de le faire à l'oral.
- C'est important pour que le jeu marche bien de jouer **à fond** pour le camp dont on fait partie à un instant T. Si vous êtes sain, vous voulez absolument éliminer la spore, en soignant ou si nécessaire en tuant tous les mutants. A l'instant où vous êtes mutæ, vous voulez absolument la victoire des mutants, et vos anciens amis les sains doivent être mutæs ou éliminæs. Si vous êtes soignæ, vous trahirez sans aucun problème vos anciens compagnons mutants. Et chaque membre de l'équipage serait prêt à sacrifier sa vie pour faire gagner son camp.
