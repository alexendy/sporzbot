_
**:desktop::fire: Lea hacker** 

Lea hacker peut aller s'introduire dans les ordinateurs des autres astronautes et consulter les fichiers confidentiels

Une fois par nuit, lea hacker se réveille après lea psychologue, lea généticien et l'informatician et décide de pirater les données d'un de ces trois rôles. Al ne peut pas pirater deux nuits de suite le même rôle. L'Ordinateur de Bord lui donne alors la même information qu'a eu ce rôle cette nuit : le nombre de mutants pour l'informatician, et la personne inspectée et le résultat de l'inspection pour lea psychologue ou lea génétician.

Le hacker pirate bien un __rôle__ (par exemple "psychologue") et non une personne ; il n'a pas besoin de savoir quæl jouaire a le rôle en question, et ne l'apprend pas. Si, pour une raison ou une autre, le rôle n'a eu aucune information ce tour ci, le hacker n'a rien non plus.

*Al y a généralement an hacker par équipage de 8 personnes ou plus*

**:detective::eye: L'espion**

L'espion peut surveiller une personne et apprendre tout ce qui lui est arrivé pendant une nuit.

L'espion se réveille en dernier et désigne une personne, sur laquelle il apprend les informations suivantes (pour la nuit passée) :
* A-t-elle été paralysée ?
* A-t-elle été mutée ?
* A-t-elle été soignée ?
* A-t-elle été inspectée par le psychologue ?
* A-t-elle été inspectée par le généticien ?

L'espion peut s'espionner lui-même. Il ne connaît pas le résultat, ni l'auteur des actions; juste si elles ont eu lieu.

*Al y a généralement un espion par équipage de 10 personnes ou plus*
