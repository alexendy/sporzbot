_
__:syringe: Tour des médecins :__
Les méecins se réveillent, et peuvent discuter dans leur canal dédié. __Un médecin paralysé ou muté ne se réveille pas__, et ne peut donc pas discuter avec son collègue, ou soigner qui que ce soit.

Chaque médecin réveillé peut soigner une personne. Si un médecin ne se réveille pas, l'autre médecin ne peut que le constater et ne sait a priori pas si son collègue a été muté ou paralysé ; dans le doute il va donc généralement utiliser son soin pour le soigner (un médecin mutant risquant de dénoncer son collègue, ce qui est une catastrophe pour les sains).

Une personne mutante qui reçoit un soin redevient saine et joue immédiatement dans le camp des sains. Exception : si la personne est hôte, elle reste mutante - elle apprend qu'elle a reçu un soin, qui a échoué à cause de son génome. Les médecins ne sont pas au courant de cet échec. Un soin n'a aucun effet sur une personne saine (mais elle sait qu'elle a reçu un soin).

*Action alternative : Tuer.* A la place de soigner, l'ensemble des médecins réveillés peuvent se concerter pour choisir de tuer une seule personne. Le meurtre remplace __tous__ les soins du tour, peu importe qu'al y ait un ou deux médecins réveillés. L'Ordinateur de Bord annonce publiquement et immédiatement que la personne a été tuée par les médecins, et elle ne peut plus être ciblée par des actions. L'autopsie n'a lieu qu'au petit matin.

__:brain::dna::computer: Tour du psychologue, du généticien et de l'informaticien :__

Ces trois rôle se réveillent tour à tour (l'ordre importe peu), sauf si als ont été paralysæs, et obtiennent de l'Ordinateur de Bord leur information. Un rôle muté continue de recevoir des informations.

__:desktop::detective: Tour du hacker et/ou de l'espion (s'ils sont présents) :__

Ces deux rôles se réveillent après les trois précédents, sauf si als ont été paralysæs, et obtiennent de l'Ordinateur de Bord leur information. Un rôle muté continue de recevoir des informations.
