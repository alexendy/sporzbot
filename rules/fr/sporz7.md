_
**::supervillain::smiling_imp: Lea traître**

Personne ne sait ce qui se passe dans la tête du traître, mais en tout cas il a décidé d'aider les mutants à répandre la spore !

Le traître est sain (initialement du moins) mais il joue toujours dans le camp des mutants, quel que soit son état de mutation. Au niveau mécanique il fonctionne exactement comme un simple astronaute; en particulier il ne connaît pas les mutants et ne se réveille pas avec eux (sauf s'il a lui-même été muté). Il gagne simplement si les mutants gagnent, et s'y emploie donc.

L'identité du traître est annoncée à l'autopsie s'il est tué.

*On peut ajouter un traître à partir de 9-10 jouaires, pour corser un peu la partie pour les sains sans pour autant rajouter un second mutant de base.*

__**Déroulement du jeu :clock1: **__

Au départ, l'Ordinateur de Bord tire au sort les rôles et les génomes, et communique à chaque personne son rôle. Il communique aussi à chaque médecin l'identité de son collègue et crée une discussion commune avec eux.

**:ballot_box: Election du chef**

La première chose à faire dans la partie est d'élire un chef (ou capitaine). Les personnes candidates se déclarent et peuvent faire un rapide discours de campagne, puis réalise un vote rapide avec des emojis (c'est un vote à main levée IRL). En cas d'égalité un second tour peut être organisé.

Le chef dispose du pouvoir de trancher les votes en cas d'égalité. Si le chef meurt, il nomme son successeur au moment de son autopsie.

Après l'élection du chef, on enchaîne immédiatement sur la première nuit.
