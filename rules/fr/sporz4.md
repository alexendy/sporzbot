_
**:biohazard::alien: Le mutant de base**

Le mutant de base est la personne contaminée initialement, qui va devoir répandre la spore dans tout le vaisseau. C'est une position importante : l'avenir de la spore repose sur vos seules épaules, du moins initialement !

Le mutant de base est toujours de génome hôte. Les sains doivent donc absolument le trouver et le tuer pour se débarrasser de la spore.

"Mutant de base" n'est pas officiellement un rôle : si le mutant de base meurt, l'autopsie indique simplement "astronaute, mutant, de génome hôte".

*Il n'y a généralement qu'un seul mutant de base, sauf à plus de dix jouaires expérimentæs.*


**:syringe::medical_symbol:  Les médecins**

Les médecins sont un rôle vital pour le camp sains : eux seuls sont capables de soigner la spore, et de ramener des mutants (non-hôtes) dans le camp des sains. Au nombre de deux, chaque médecin réveillé peut soigner une personne par nuit. Si les mutants découvrent l'identité des médecins, les chances de victoire des sains sont généralement serieusement compromises.

Les médecins sont toujours de génome normal. Dissimuler leur identité aux yeux des mutants doit être une priorité pour eux-mêmes et pour les sains, et la découvrir est un objectif des mutants.

*Il y a deux médecins, qui travaillent ensemble et se connaissent initialement.*
