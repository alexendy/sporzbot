_
**:brain::notebook_with_decorative_cover:  Lea psychologue**

La spore est presque indétectable... mais presque n'est pas complètement. Elle affecte légèrement le comportement des personnes contaminées. Ce changement est trop subtil pour être détecté par la plupart des gens, mais il n'échappe pas à l'oeil d'an psychologue bien formé.

Une fois par nuit, le psychologue peut analyser le comportement d'une personne, et découvrir si cette personne est mutante ou saine.

*Il y a an psychologue par équipage*


**:scientist::dna: Lea génétician**

Lea génétician est capable d'analyser le génome de l'équipage pour connaître sa résistance à la spore.

Une fois par nuit, lea génétician peut analyser le génome d'une personne, et découvrir si cette personne est normale, résistante ou hôte. Al peut si al le souhaite s'auto-inspecter.

*Al y a an génétician par équipage*


**:computer::zero::one: L'informaticien**

L'informatician est capable, par de savants calculs statistiques, d'estimer le nombre de mutants à bord.

Une fois par nuit, l'Ordinateur de Bord indique à l'informatician le nombre de mutants dans l'équipage.

*Al y a an informatician par équipage*
