_
__**Personnages**__

Un personnage est défini par trois caractéristiques :
- Son **état** : sain ou mutant. Il est suceptible de varier pendant la partie suite aux mutations ou aux soins.
- Son **génome** : tout le monde n'est pas égal face à la spore. Votre génétique vous range dans l'une des trois catégories suivantes :
  * génome **résistant** : vous avez une immunité naturelle à la spore, et ne pouvez jamais être mutæ !
  * génome **hôte** : votre génétique vous empêche de combattre la spore. Vous n'êtes pas (forcément) mutanx initialement, mais si vous le devenez, vous ne pouvez jamais être soignæ !
  * génome **normal** : vous pouvez être mutæ ou soignæ normalement
On ne connaît généralement pas son propre génôme en début de partie. Le mutant de base est hôte, et al existe un autre hôte (sain au départ) et un résistant dans le reste de l'équipage.
- Son **rôle** : certaines personnes ont un rôle qui leur donne des capacités ou propriétés supplémentaires, détaillé ci-dessous:

Quand un personnage meurt une autopsie est pratiquée et son état, son rôle et son génome sont annoncés publiquement par l'Ordinateur de Bord (immédiatement de jour, au petit matin si la mort à lieu la nuit).

**:astronaut::rocket: Les astronautes**

Les astronautes sont des membres de l'équipage sans rôle particulier. Toux jouaire qui n'a pas un autre rôle est astronaute.
