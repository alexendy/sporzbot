_
**:crescent_moon::night_with_stars: La nuit**

Personne ne parle pendant la nuit, sauf cas prévus par les règles. La nuit se déroule comme suit :

__:biohazard: Tour des mutants :__
Les mutants se réveillent : l'Ordinateur de Bord crée une discussion avec tous les mutants du tour, qui peuvent se concerter par écrit pour décider quoi faire. Les mutants font deux actions par nuit : *paralyser* et *muter* une personne. Quel que soit le nombre de mutants présents, on paralyse une seule personne et on mute une seule personne. 
* Une personne paralysée ne joue pas son rôle pour cette nuit : elle ne se réveille pas à son tour, et ne peut faire de soin si elle est médecin ou obtenir des informations, etc. Sur un simple astronaute la paralysie n'a aucun effet, mais la personne sait quand même qu'elle a été paralysée.
* Une personne mutée rejoint le camp des mutants après le tour des mutants en cours. Elle ne sait pas qui l'a mutée. Exception : si la personne mutée est de génome résistant, elle reste saine - mais elle apprend que les mutants ont tenté de la muter et que cela a échoué. Les mutants ne sont pas au courant de cet échec.

On peut muter et paralyser la même personne, mais c'est rare. Les mutants peuvent se paralyser eux-mêmes. Les mutants doivent décider autant que faire se peut à l'unanimité et au consensus.

*Action alternative : Tuer.* A la place de muter, les mutants peuvent choisir de tuer une personne. L'Ordinateur de Bord annonce publiquement et immédiatement que la personne a été tuée par les mutants, et elle ne peut plus être ciblée par des actions. L'autopsie n'a lieu qu'au petit matin.
