__**:biohazard: SPORZ :biohazard:**__

*Dans un futur lointain, un immense vaisseau spatial s'élance dans l'espace, emportant vers un monde détecté comme habitable des milliers de colons en hypersommeil. Le vaisseau est largement automatisé; seul un équipage d'une dizaine de personnes est présent, et il n'est réveillé que pour superviser le débarquement, ou en cas de crise.*

*C'est pour cette dernière éventualité que l'Ordinateur de Bord vous a réveillæ : une crise. Dès que vous vous extrayez du cocon de fugue cryotechnique, vous entendez l'alarme. ":red_circle: ALERTE ! AGENT BIOLOGIQUE MUTAGENE DETECTE ! EQUIPAGE CONTAMINE ! PROTOCOLE DE CONFINEMENT ACTIF !"*

*Avec vos collègues, vous prenez conscience de la gravité de la situation. L'an d'entre vous a été contaminæ par une **spore mutante**, un terrible agent pathogène presque indétectable, qui pousse ses portaires à contaminer toujours plus de personnes... Vous vous observez les ans les autres. Qui est contaminæ ? Le découvrir avant l'arrivée du vaisseau et contenir la contamination est indispensable, ou c'est tout un monde qui sera perdu à la Spore...*
