_
**:sun_with_face::city_sunset: Le jour**

Au petit matin, si des gens sont morts pendant la nuit, l'autopsie est pratiquée et leur rôle, état et génome est annoncé publiquement par l'Ordinateur de Bord. Si le chef est mort de cette façon, il nomme immédiatement un successeur.

Pendant la journée, l'équipage discute librement à sa guise, en public, en petits groupes, en privé... en texte ou en vocal. (Merci d'inclure l'Ordinateur de Bord dans les messages privés.) Le but est d'échanger les informations avec son camp, de tenter d'identifier les mutants, et de décider de son vote, qui décidera si une personne sera exécutée par l'équipage aujourd'hui.

Le vote se fait à bulletin secret par message à l'Ordinateur de Bord. Trois possibilité existent :
* Voter contre an membre de l'équipage : s'il a la majorité, il sera tué
* Voter blanc, c'est à dire voter pour ne tuer personne : si blanc a la majorité, personne n'est tué
* S'abstenir et ne pas prendre part au vote

Dès que tout le monde a voté, l'Ordinateur comptabilise les votes et publie les résultats. En cas d'égalité, le chef tranche entre les options ayant récolté le plus de voix.

Si quelqu'an a été tuæ, son autopsie est publiée par l'Ordinateur de Bord. Puis, la nuit suivante tombe __immédiatement__ (pas d'autres discussions).
