_
**Conditions de victoire**
Elles sont simples :
- Si al n'y a plus aucun mutant à bord, les astronautes ont gagné. 
- Si tous les personnages en vie sont mutants, les mutants ont gagné.

Al est courant que les sains concèdent la partie si la situation est trop désespérée pour elleux, en particulier si les deux médecins sont morts ou mutés.

Les personnages "gagnent" et "perdent" avec le camp dont als font partie à la fin de la partie. (Le traître gagne et perd avec les mutants quel que soit son état de mutation.) Un personnage mort gagne ou perd avec le camp dont il faisait partie à sa mort.
