from sporzbot import bot

import json

with open("config.json",'r') as fd:
    config = json.load(fd)

token = config["token"]

bot.run(token)
