from setuptools import setup, find_packages

setup(name='sporzbot',
      install_requires=['discord.py','aiofiles'],
      version='0.1.1',
      packages=find_packages(),
      include_package_data=True,
      author='Miranda Coninx',
      author_email='miranda@coninx.org'
)
